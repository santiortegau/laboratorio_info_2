#include <iostream>

using namespace std;

/* This program gets an odd number and prints a diamond pattern.
 * REQUIREMENTS:
 *  -Odd Number greater than zero.
 * GUARANTEES:
 *  -Prints a diamond pattern composed by asterisks
 */

int main()
{
    int n, contLineas=0, astperLinea, contEspacios, lineActual, espacImpresos=0, astImpresos=0;
    cout<<"Ingrese un numero Impar mayor que cero: "<<endl;
    cin>>n;
    while (n<0 || n%2==0){
        cout<<"Ingrese un numero Impar mayor que cero: "<<endl;
        cin>>n;
    }
    lineActual=0;
    astperLinea=1;
    contEspacios=(n-astperLinea)/2;                     //Define the maximum quantity of spaces per line.
    while (lineActual<(n/2)+1){                         //This while prints the pattern until it reaches the middle
        while (lineActual==contLineas){                 //This statement prints while on the specified line
            astImpresos=0;
            espacImpresos=0;
            while (espacImpresos<contEspacios){
                espacImpresos++;
                cout<<" ";
            }
                while (astImpresos<astperLinea){
                    astImpresos++;
                    cout<<"*";
                }
                lineActual++;
        }
        cout<<endl;
        contLineas++;
        contEspacios-=1;
        astperLinea+=2;
    }
    astperLinea=n-2;                                     //The middle+1 line will have the previous odd number in asterisks
    contEspacios=(n-astperLinea)/2;
    while (lineActual<n){                                //The printing process will end when it reaches the desired number
        while (lineActual==contLineas){
            astImpresos=0;
            espacImpresos=0;
            while (espacImpresos<contEspacios){
                espacImpresos++;
                cout<<" ";
            }
                while (astImpresos<astperLinea){
                    astImpresos++;
                    cout<<"*";
                }
                lineActual++;
        }
        cout<<endl;
        contLineas++;
        contEspacios+=1;                                 //Now you want to increment spaces per line and decrement asterisks per line.
        astperLinea-=2;
    }
    return 0;
}
