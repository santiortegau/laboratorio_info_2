#include <iostream>

using namespace std;
/* This program gets an integer number and returns the sum of all its digits powered to themselves.
 * REQUIREMENTS:
 *  -Integer number greater than zero.
 * GUARANTEES:
 *  -The sum of all its digits powered to themselves.
 */


int main()
{
    long int n,sum=0;                                   //Define long integers given that the maximum powered value is 9
    cout<<"Ingrese un numero entero: "<<endl;
    cin>>n;
    while (n!=0){                                       //In this loop, each digit is compared and its powered value added to the result
        switch (n%10) {
        case 1:
            sum+=n%10;
            break;
        case 2:
            sum+=(n%10)*(n%10);
            break;
        case 3:
            sum+=(n%10)*(n%10)*(n%10);
            break;
        case 4:
            sum+=(n%10)*(n%10)*(n%10)*(n%10);
            break;
        case 5:
            sum+=(n%10)*(n%10)*(n%10)*(n%10)*(n%10);
            break;
        case 6:
            sum+=(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10);
            break;
        case 7:
            sum+=(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10);
            break;
        case 8:
            sum+=(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10);
            break;
        case 9:
            sum+=(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10)*(n%10);
            break;
        }
        n/=10;                                          // N is assigned a new value minus the last digit added.

    }
    cout<<"El resultado de la suma es: "<<sum<<endl;


}
