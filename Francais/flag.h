#ifndef FLAG_H
#define FLAG_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QMessageBox>
#include <iostream>
#include <QString>
using namespace std;

class flag : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit flag(QObject *parent = nullptr);
    void changeFlag(int flagNo);
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    QString name;

signals:

public slots:
};

#endif // FLAG_H
