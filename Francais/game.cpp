#include "game.h"
#include "ui_game.h"

game::game(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::game)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(35,40,1300,600);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/new/prefix1/back.png")));
    crono= new QTime();
    timer= new QTimer();
    crono->start();
    timer->start();
    connect(timer,SIGNAL(timeout()),this,SLOT(timeLeft()));

}

game::~game()
{
    delete ui;
    delete crono;
    delete scene;
}

void game::showFlags()
{

}

void game::timeLeft()
{
    ui->lcdNumber->display(120-crono->elapsed()/1000);

}

void game::on_pushButton_clicked()
{
    timer->stop();
    for(int i=0;i<6;i++){
        flag *banderas=new flag();
        banderas->setScale(0.2);
        if(i==0){
            banderas->setPos(500,300);
            banderas->changeFlag(1);
        }
        if(i==1){
            banderas->setPos(640,300);
            banderas->changeFlag(2);
        }
        if(i==3) banderas->setPos(780,300);
        if(i==4) banderas->setPos(500,420);
        if(i==5) banderas->setPos(640,420);
        if(i==2) banderas->setPos(780,420);
        scene->addItem(banderas);
    }

}

void game::nigerClicked()
{


}
