#include "flag.h"

flag::flag(QObject *parent) : QObject(parent)
{
    setPixmap(QPixmap(":/new/prefix1/niger.png"));
    this->setFlags(QGraphicsItem::ItemIsSelectable);
}

void flag::changeFlag(int flagNo)
{
    if (flagNo==1) {
        setPixmap(QPixmap(":/new/prefix1/niger.png"));
        name="Niger";
    }
    if (flagNo==2){
        setPixmap(QPixmap(":/new/prefix1/seychelles.png"));
        name="seychelles";
    }


}

void flag::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QMessageBox::information(NULL, "Information!", name);
    this->ungrabMouse();
}
