#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <qgraphicsscene.h>
#include <QTime>
#include <QTimer>
#include <QList>
#include "flag.h"
#include <qmessagebox.h>

namespace Ui {
class game;
}

class game : public QWidget
{
    Q_OBJECT

public:
    explicit game(QWidget *parent = nullptr);
    ~game();
    void showFlags();

public slots:
    void timeLeft();

private slots:
    void on_pushButton_clicked();
    void nigerClicked();

private:
    Ui::game *ui;
    QGraphicsScene *scene;
    QTime *crono;
    QTimer *timer;
    QTimer *niger;
    QList <flag*> flagList;
};

#endif // GAME_H
