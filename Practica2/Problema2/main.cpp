#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;
/* This program generates a random capital letters array containing 200 letters, and calculates how many times each
 * letter is repeated.
 * REQUIREMENTS:
 *  -None.
 * GUARANTEES:
 *  -Prints the array and each letter of the alphabet with its respective number of repetitions.
 */
void compare(int a[]); //Define each function prototype.
int gen(int array[]);

int main()
{
    srand(time(0));     //Seed random function to the CPU actual time in seconds
    int array[200];     //Generate the array with 200 positions
    gen(array);
    for(int k=0;k<=199;k++){
        cout<<char (array[k]);
    } cout<<endl;
    compare(array);
}

void compare(int a[]){      //This function compares each letter
    for (int i=65; i<=90;i++){ //For Capital letters
        int k=0;
        for(int j=0;j<=199;j++){ //Step on each position of the array
            if (a[j]==i) k++; //Increment counter and print its value
        }
        cout<<char (i)<<": "<<k<<endl;
    }
}

int gen(int array[]){       //This function generates the array
    for(int i=0;i<=199;i++){
        array[i]=(rand()%25+65); //To each position it assigns a random value from 65 to 90
    }
    return *array;
}
