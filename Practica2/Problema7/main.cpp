#include <iostream>

using namespace std;
string rep(string);
string del(string, string);
int main()
{
    string ppal,repetidas,sinRepetidas;
    cout<<"Ingrese una cadena de caracteres: "<<endl;
    cin>>ppal;
    repetidas=rep(ppal);
    sinRepetidas=del(repetidas,ppal);
    cout<<"Original:"<<ppal<<". Sin repetidas: "<<sinRepetidas<<endl;

}
string rep(string str){
    string rep="";
    signed int found;
    for (unsigned int i=0; i<str.length();i++){
        for(unsigned int j=str.length();j>0&&j>i;j--){
            if (str[i]==str[j]){
                found=rep.find(str[i]);
                if (found==-1){
                    rep+=str[i];
                }

            }
        }
    }
    return rep;
}

string del(string rep, string a){
    signed int found;
    string solution;
    for(unsigned int i=0;i<a.length();i++){
        found=rep.find(a[i]);
        if (found==-1) solution+=a[i];
    }
    return solution;
}
