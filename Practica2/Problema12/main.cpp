#include <iostream>

using namespace std;
/* This program gets a square matrix, prints it and checks if it's a Magic Square
 * REQUIREMENTS:
 *  -Each element of the matrix and its dimension
 * GUARANTEES:
 *  -Returns the the input matrix and if it's a Magic Square or not.
 */
void **matFiller(int n);
int verMagSq(int **,int);
int main()
{
    int n;
    cout<<"Ingrese la dimension de la matriz cuadrada: "<<endl;
    cin>>n;
    matFiller(n);
    return 0;
}

void **matFiller(int n){ //This function asks for a matrix input and gets the dimension of the square matrix
    int ** matrix=new int*[n];  //Create a double pointer which allocates dinamically a pointer to an array
    for (int i=0;i<n;i++){      //For each position in the matrix assign an array of n dimensions
        matrix[i]=new int[n];
    }
    int num;
    for (int i=0;i<n;i++){  //For each row
        for(int j=0;j<n;j++){ //For each column
            cout<<"Ingrese el valor de la posicion ("<<i+1<<", "<<j+1<<"): "<<endl;
            cin>>num;
            *(*(matrix+i)+j)=num; //Assign each row i and j column the input value
        }
    }
    for (int i=0;i<n;i++){
        for (int j=0;j<n;j++){
            if(*(*(matrix+i)+j)<10){ //Print the matrix and add spaces if the number is greater than 9 to print it as a square matrix
                cout<<" "<<*(*(matrix+i)+j)<<" ";
            }
            else{
                cout<<*(*(matrix+i)+j)<<" ";
            }
        }
        cout<<endl;
    }

    verMagSq(matrix,n); //Invoke function to check Magic Square
    for (int i=0;i<n;i++){
        delete [] matrix[i];
    }
    delete [] matrix; //Delete matrix from the heap
}
int verMagSq(int **matrix,int n){ //Gets the double pointer from matrix and its dimensions
    int index=0,sum,diag=0,antiDiag=0;
    bool isMagicSq=true; //Flag to determine if Magic Square
    for (int i=0;i<n;i++){ //Calculates a reference sum
        index+=*(*(matrix+i));
    }
    for (int i=0;i<n;i++){ //Calculates the sum of each row
        sum=0;
        for (int j=0;j<n;j++){
           sum+=*(*(matrix+i)+j);
        }
        if(sum!=index){
            isMagicSq=false;
        }
    }
    for (int i=0;i<n;i++){ //Calculates the sum of each column
        sum=0;
        for (int j=0;j<n;j++){
           sum+=*(*(matrix+j)+i);
        }
        if(sum!=index){ //If sum is not equal to the reference sum the matrix is not a Magic Square
            isMagicSq=false;
        }
    }
    for (int i=0;i<n;i++){; //Calculates the sum of the diagonal
        diag+=*(*(matrix+i)+i);
    }
    for (int i=0;i<n;i++){ //Calculates the sum of the antidiagonal
        antiDiag+=*(*(matrix+(n-1-i))+i);
    }
    if(diag!=index||antiDiag!=index){
        isMagicSq=false;
    }
    if (isMagicSq) cout<<"Es un Cuadrado Magico. "<<endl;
    else cout<<"No es un Cuadrado Magico. "<<endl;
    return 0;
}
