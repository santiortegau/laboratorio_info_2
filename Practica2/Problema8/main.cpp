#include <iostream>

using namespace std;
/* This program gets an alphanumeric string and splits numbers from letters.
 * REQUIREMENTS:
 *  -String with ASCII alphanumeric characters.
 * GUARANTEES:
 *  -Returns the original string and letters separated from numbers
 */
string nums(string);
string letters(string);
int main()
{
    string str,numeros,letras;
    cout<<"Ingrese una cadena de caracteres alfanumerica:"<<endl;
    cin>>str;
    numeros=nums(str);
    letras=letters(str);
    cout<<"Texto: "<<letras<<". Numeros: "<<numeros<<endl;
}
string nums(string str){
    string numsInStr=""; //Empty string to return
    for (unsigned int i=0;i<str.length();i++) //Go over each position of the string
        if (char (str[i])>47 && char (str[i])<58){ //If the value in i position is a number fill the empty string
            numsInStr+=str[i];
        }
    return numsInStr;
}
string letters(string str){
    string lettersInStr=""; //Empty string to return
    for (unsigned int i=0;i<str.length();i++)
        if ((char (str[i])>=65 && char (str[i])<91) || (char (str[i])>=97 && char (str[i])<123)){ //If the character is a letter fill the string
            lettersInStr+=str[i];
        }
    return lettersInStr;

}
