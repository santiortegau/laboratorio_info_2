#include <iostream>
#include<math.h>

using namespace std;
/* This program gets a string with numerical characters and converts them into integers.
 * REQUIREMENTS:
 *  -String with numerical characters
 * GUARANTEES:
 *  -Returns an integer value.
 */

int convert(string str);

int main()
{
    string str;
    cout<<"Ingrese una cadena de numeros: "<<endl;
    cin>>str;
    cout<< "El numero ingresado es: "<< convert(str)<<endl;

}
int convert(string str){    //This is the main function which converts string to int.
    unsigned int length=str.length(), suma=0; //Length defines the number of digits
    unsigned int exp=length;        //exponent to power in base 10
    for (int i=0;i<str.length();i++){
        exp=length-1;
        suma+=(int(str[i])-48)*pow(10,exp); //ASCII value minus 48 returns the integer, powered to its position in base 10
        length--;
    }
    return suma;
}
