#include <iostream>

using namespace std;
/* This program rotates a 5x5 matrix
 * REQUIREMENTS:
 *  -Degrees to rotate the matrix
 * GUARANTEES:
 *  -Returns the original matrix and its clockwise rotated counterpart to the preset degree
 */
void printMat();
void rotateMat(int);
int main()
{
    int deg;
    cout<<"Ingrese los grados a rotar la matriz (90,180,270): "<<endl;
    cin>>deg;
    printMat();
    rotateMat(deg);
    return 0;
}
void printMat(){ //This function prints the original matrix
    int cont=1;
    int mat[5][5];
    for (int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            mat[i][j]=cont;
            cont++;
        }
    }
    cout<<"Matriz Original:"<<endl;
    for (int i =0;i<5;i++){              //For each row and each column print the matrix
        for (int j=0;j<5;j++){
            if ((mat[i][j])<10){         //If the number to print is less than 10, add spaces in between to make it appear as a squared matrix
                cout<<" "<<mat[i][j]<<" ";
            }
            else{
                cout<<mat[i][j]<<" ";
            }
        }
        cout<<endl;
    }
}
void rotateMat(int deg){ //This function rotates the matrix
    switch (deg) { //Given the degrees to rotate, execute the correct statement
    case 90:{
        int cont=1;
        int mat[5][5];
        for (int j=4;j>=0;j--){ //Start filling the matrix from top right to bottom right
            for(int i=0;i<5;i++){
                mat[i][j]=cont;
                cont++;
            }
        }
        cout<<endl<<"Matriz Rotada 90 grados: "<<endl;
        for (int i =0;i<5;i++){              //For each row and each column print the matrix
            for (int j=0;j<5;j++){
                if ((mat[i][j])<10){         //If the number to print is less than 10, add spaces in between to make it appear as a squared matrix
                    cout<<" "<<mat[i][j]<<" ";
                }
                else{
                    cout<<mat[i][j]<<" ";
                }
            }
            cout<<endl;
        }
        break;
    }
     case 180:{
        int cont=1;
        int mat[5][5];
        for (int j=4;j>=0;j--){ //Start filling the matrix from bottom right to bottom left
            for(int i=4;i>=0;i--){
                mat[j][i]=cont;
                cont++;
            }
        }
        cout<<endl<<"Matriz Rotada 180 grados: "<<endl;
        for (int i =0;i<5;i++){              //For each row and each column print the matrix
            for (int j=0;j<5;j++){
                if ((mat[i][j])<10){         //If the number to print is less than 10, add spaces in between to make it appear as a squared matrix
                    cout<<" "<<mat[i][j]<<" ";
                }
                else{
                    cout<<mat[i][j]<<" ";
                }
            }
            cout<<endl;
        }
        break;
    }
    case 270:{
        int cont=1;
        int mat[5][5];
        for (int j=0;j<5;j++){ //Start filling the matrix from bottom left to top left
            for(int i=4;i>=0;i--){
                mat[i][j]=cont;
                cont++;
            }
        }
        cout<<endl<<"Matriz Rotada 270 grados: "<<endl;
        for (int i =0;i<5;i++){              //For each row and each column print the matrix
            for (int j=0;j<5;j++){
                if ((mat[i][j])<10){         //If the number to print is less than 10, add spaces in between to make it appear as a squared matrix
                    cout<<" "<<mat[i][j]<<" ";
                }
                else{
                    cout<<mat[i][j]<<" ";
                }
            }
            cout<<endl;
        }
        break;
    }
    }
}
