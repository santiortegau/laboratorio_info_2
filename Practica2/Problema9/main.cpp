#include <iostream>
#include <math.h>
#include <algorithm>

using namespace std;
int split(string &str,int n);
int convert(string str);
int main()
{
    string num;
    int result,n;
    cout<<"Ingrese un numero: "<<endl;
    cin>>num;
    cout<<"Ingrese un numero n para separar: "<<endl;
    cin>>n;
    result=split(num,n);
    cout<<"Original: "<<num<<endl;
    cout<<"Suma: "<<result<<endl;
}
int split(string &str,int n){
    string numsInN="";
    int cont=0,suma=0,nums;
    for (int i=(str.length())-1;i>=0;i--){
        if (cont<n){
            numsInN+=str[i];
            cont++;
        }
        else {
            reverse(numsInN.begin(),numsInN.end());
            nums=convert(numsInN);
            suma+=nums;
            cont=0;
            numsInN="";
            i++;
        }
    }
    if (numsInN.length()<n){
        string newStr=str;
        newStr=str.substr(0,n-1);
        newStr.insert(0,"0");
        suma+=convert(newStr);
    }
    if (n==1&&numsInN=="1")
        suma++;
    return suma;
}
int convert(string str){
    unsigned int length=str.length(), suma=0;
    unsigned int exp=length;
    for (int i=0;i<str.length();i++){
        exp=length-1;
        suma+=(int(str[i])-48)*pow(10,exp);
        length--;
    }
    return suma;
}
