#include <iostream>

using namespace std;
/* This program gets a string and changes lower case letters to upper case.
 * REQUIREMENTS:
 *  -String with ASCII characters.
 * GUARANTEES:
 *  -Returns the same string with all its letters in upper case, not affecting other characters
 */
void mayuscula(string a);
int main()
{
    string str;
    cout<<"Ingrese una cadena de caracteres: "<<endl;
    cin>>str;
    cout<<"Original: ";
    for(int i=0;i<str.length();i++){    //Prints the string
        cout<<char (str[i]);
    }
    cout<<". En mayuscula: ";
    mayuscula(str);

}
void mayuscula(string a){
    unsigned int letra;
    string salida;
    for (int i=0;i<a.length();i++){
        letra=int (a[i]); //Letra gets the ascii value for each letter
        if (letra>96&&letra<123){ //If the letter is lowercase decrease its value 32 units, resulting in its upper case value
            letra-=32;
            salida=char(letra);
            cout<<salida;
        }else{ //If not lower case or letter, print it unaffectedly
            salida=char(letra);
            cout<<salida;

         }
    }cout<<endl;
}
