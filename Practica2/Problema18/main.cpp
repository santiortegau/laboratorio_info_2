#include <iostream>
#include <bits/stdc++.h>

using namespace std;
/* This program gets an n number and calculates the nth lexicographic permutations of numbers from 0 to 9
 * REQUIREMENTS:
 *  -n number
 * GUARANTEES:
 *  -Returns the nth lexicographic permutation of numbers from 0 to 9
 */
string nthPerm(string str, long int n);
int main()
{
    int n;
    cout<<"Ingrese un numero para hallar la n-esima permutacion: "<<endl;
    cin>>n;
    cout<<"La permutacion numero "<<n<<" es: "<< nthPerm("0123456789",n)<<endl;
}
string nthPerm(string str, long int n) //N can be a long integer
{
    long int i = 1;
    do {
        if (i == n)
            break;

        i++;
    } while (next_permutation(str.begin(), str.end())); //Increase i till it reaches nth iteration and use next permutation to clculate next sorted permutation of the string

    // print string after nth iteration
    return str;
}
