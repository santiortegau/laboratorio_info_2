#include <iostream>

using namespace std;
/* This program gets an n number and calculates the number of paths in an nxn grid
 * REQUIREMENTS:
 *  -n number
 * GUARANTEES:
 *  -Returns the number of paths (only moving to the right and down) in a nxn grid
 */
int  numberOfWays(int m, int n);
int main()
{
    int n;
    cout<<"Ingrese una dimension para la matriz: "<<endl;
    cin>>n;
    cout<<"Para una malla de "<<n<<"x"<<n<<" puntos hay "<<numberOfWays(n,n)<<" caminos."<<endl;
}
int  numberOfWays(int m, int n)
{
   if (m == 0 || n == 0) //Whenever you reach a stop within the grid return 1 path
        return 1;
   return  numberOfWays(m-1, n) + numberOfWays(m, n-1); //Recursively use the function again decreasing movements to the left and upwards
}
