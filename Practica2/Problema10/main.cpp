#include <iostream>
using namespace std;
/* This program converts a roman number to its arabic counterpart
 * REQUIREMENTS:
 *  -String with upper case roman numbers
 * GUARANTEES:
 *  -Returns the the original roman number and its arabic counterpart.
 */
int main()
{
    char numRomano;
    int numArabico=0;

    cout<<"Ingrese el numero romano en Mayuscula: ";
    while(cin.get(numRomano)) //Get each character from the terminal
    {
        if (numRomano == 'M') numArabico=numArabico+1000;

        else if (numRomano == 'D')
        {
            numRomano = cin.peek(); //Assigns to numRomano the next char in the input sequence without changing it
            if(numRomano == 'M'){ numArabico=numArabico-500; continue;}
            else{numArabico=numArabico+500; continue;}
        }

        else if (numRomano == 'C')
        {
            numRomano = cin.peek();
            if(numRomano == 'M' || numRomano== 'D'){ numRomano=numRomano-100; continue;}
            else{numArabico=numArabico+100; continue;}
        }

        else if (numRomano == 'L')
        {
            numRomano = cin.peek();
            if(numRomano == 'M' ||numRomano == 'D' || numRomano == 'C'){ numArabico=numArabico-50; continue;}
            else{ numArabico=numArabico+50; continue;}
        }

        else if (numRomano == 'X')
        {
            numRomano = cin.peek();
            if(numRomano == 'M' || numRomano == 'D' || numRomano == 'C' || numRomano == 'L'){ numArabico=numArabico-10; continue;}
            else{numArabico=numArabico+10; continue;}
        }

        else if (numRomano == 'V')
        {
            numRomano = cin.peek();
            if(numRomano == 'M' || numRomano == 'D' || numRomano == 'C' || numRomano == 'L' || numRomano== 'X'){ numArabico=numArabico-5; continue;}
            else{ numArabico=numArabico+5; continue;}
        }

        else if (numRomano == 'I')
        {
            numRomano = cin.peek();
            if(numRomano == 'M' || numRomano == 'D' || numRomano == 'C' || numRomano == 'L' || numRomano == 'X' || numRomano == 'V'){numArabico=numArabico-1; continue;}
            else{numArabico=numArabico+1; continue;}
        }
        else break;
    }
cout<<numArabico<<endl;
return 0;
}
