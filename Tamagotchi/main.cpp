#include <iostream>
#include <string.h>
#include "tamagotchi.h"


using namespace std;

int main()
{
    bool flag=false;
    int option=0;
    cout<<"Bienvenido a Tamagotchi!"<<endl;
    cout<<"Presione 1 para empezar a jugar: "<<endl;
    cin>>option;
    if (option!=1){
        cout<<"Presione 1 para empezar a jugar: "<<endl;
    }
    flag=true;
    while (flag){
        Tamagotchi tama;
        tama.stats();
        string fruit="Fruta";
        food manzana(10,6,fruit);
        food pera(10,2,fruit);
        string meal="Comida";
        food carne(10,8,meal);
        food pollo(10,10,meal);
        mouth mouth;
        cout<<endl<<"Desea alimentar a "<<tama.getname()<<"? (1. Si/2. No)"<<endl;
        int select;
        cin>>select;
        int end=1;
        while(end==1){
            if (select==2){
                tama.size--;
                tama.age+=5;
                tama.state-=5;
                tama.vida-=10;
                if(tama.vida==0||tama.state==0||tama.age==100||tama.size>20){
                    cout<<tama.name<<" ha muerto. :( "<<endl;
                    flag=false;
                    break;
                }

                tama.stats();

             }
            else if(select==1){
                cout<<"Usted posee: "<<endl<<manzana.quant<<" Manzanas, ";
                cout<<pera.quant<<" Peras"<<", ";
                cout<<carne.quant<<" Carne"<<", ";
                cout<<pollo.quant<<" Pollo"<<endl;
                cout<<endl<<"Elija la comida que quiere dar: "<<endl;
                cout<<"1. Manzana, 2. Pera, 3. Carne, 4. Pollo"<<endl;
                int select2;
                cin>>select2;
                if (select2==1){
                    mouth.chew(manzana);
                    manzana.setQuant();
                    mouth.swallowing();
                    if (tama.vida<100) tama.vida+=manzana.nutValue;

                }
                else if (select2==2){
                    mouth.chew(pera);
                    pera.setQuant();
                    mouth.swallowing();
                    if (tama.vida<100) tama.vida+=pera.nutValue;
                }
                else if(select2==3){
                    mouth.chew(carne);
                    carne.setQuant();
                    mouth.swallowing();
                    if (tama.vida<100) tama.vida+=carne.nutValue;
                }
                else if (select2==4){
                    mouth.chew(pollo);
                    pollo.setQuant();
                    mouth.swallowing();
                    if (tama.vida<100) tama.vida+=pollo.nutValue;
                }
                tama.size++;
                tama.age+=5;
                if (tama.state<100) tama.state+=5;
                tama.hunger=false;

                if(tama.vida==0||tama.state==0||tama.age==100||tama.size>20){
                    cout<<tama.name<<" ha muerto. :( "<<endl;
                    flag=false;
                    break;
                }

                tama.stats();

            }
            cout<<endl<<"Desea alimentar a "<<tama.getname()<<"? (1. Si/2. No)"<<endl;

            cin>>select;
        }



    }

}

