#ifndef TAMAGOTCHI_H
#define TAMAGOTCHI_H

#include <iostream>
using namespace std;

class Tamagotchi
{
public:
    Tamagotchi();
    void setname();
    string getname();
    void crecer();
    bool hunger;
    int mood();
    void stats();
    int vida,age,state,size;
    string name;
};

class food{
public:
    food(float q,int nV,string type);
    float quant;
    int nutValue;
    string foodType;
    void setQuant();
    void stats();

};
class mouth{
private:
    float capacity;
    bool chewing;
public:
    mouth();
    bool chew(food &);
    bool swallowing();
};
class stomach{
private:
    float capacity;
    bool processed;
    int digestVelocity;
public:
    void process(bool chewing,Tamagotchi);
};

#endif // TAMAGOTCHI_H
