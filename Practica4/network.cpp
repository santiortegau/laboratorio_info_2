#include "network.h"


void Router::setName(string str){
    name=str;
}
string Router::getName(){
    return name;
}
void Router::addLink(string router1,int cost){
    Links.insert({router1,cost});
}

void Router::removeLink(string router){
    Links.erase({router});
}

Network::Network()
{
}

void Network::addRouter(Router rtr){
    routers=rtr;
    updateTable(rtr);
}

void Network::updateTable(Router rtr){
    routing_table.push_back(rtr);
}
void Network::deleteRouter(Router rtr){
    for (unsigned int i=0;i<routing_table.size();i++){
        if (routing_table[i].getName() == rtr.getName()){
            routing_table.erase(routing_table.begin()+i);
        }
    }
}

