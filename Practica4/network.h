#ifndef NETWORK_H
#define NETWORK_H

#include<iostream>
using namespace std;
#include <vector>
#include <map>
#include <list>

class Router
{
public:
    void setName(string str);
    string getName();
    void addLink(string router1, int cost);
    void removeLink(string router);
    map <string,int>Links;

private:
    string name;
};

class Network
{
public:
    Network();
    void addRouter(Router);
    vector <Router> routing_table;
    void updateTable(Router);
    void deleteRouter(Router);
    void generateNet(); //Pendiente
    int queryCost(Router,Router);
    map <string,int> queryPath(string, string);

private:
    Router routers;



};

#endif // NETWORK_H
