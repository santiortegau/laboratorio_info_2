#include <iostream>
#include <network.h>
#include <vector>
#include <fstream>
#include <queue>
#include <list>

using namespace std;
map <string,int> queryPath (Network,string);
string path (Network net,string origin,string destination);

int main()
{
    Network initNet;
    string name;
    while (true){
        cout<<"NETWORK CONFIG"<<endl<<endl;
        cout<<"Choose one of the following options: "<<endl;
        cout<<"1. Add Routers/Links."<<endl<<"2. Remove Routers/Links."<<endl<<"3. Query cost between Routers."<<endl<<"4. Query best path between Routers."<<endl<<"5. Load NET from file."<<endl;
        cout<<"6. To END Network Config"<<endl;
        int option;
        cout<<"Input (ONLY USE INTEGER NUMBERS): ";
        cin>>option;
        cout<<endl<<endl;
        if (option==1){
            int option1;
            cout<<"1. Add Router."<<endl;
            cout<<"2. Add Links between routers."<<endl<<"0. Return."<<endl;
            cout<<"Input: ";
            cin>>option1;
            cout<<endl;
            if (option1==1){
                Router tempRouter;
                cout<<"Enter the name: "<<endl;
                cin>>name;
                tempRouter.setName(name);
                bool alreadyExists=false;
                for (unsigned int i=0;i<initNet.routing_table.size();i++){
                        if(initNet.routing_table[i].getName()== tempRouter.getName()){
                            alreadyExists=true;
                            cout<<"This router already exists."<<endl;
                            break;
                        }
                    }
                if (!alreadyExists) initNet.addRouter(tempRouter);
            }
            if (option1==2){
                string destination,origin;
                int cost;
                cout<<"Available Routers in the Network:"<<endl;
                for (unsigned int i=0;i<initNet.routing_table.size();i++){
                    cout<<initNet.routing_table[i].getName()<<endl;
                }
                cout<<"Enter names of the Routers to be linked: "<<endl;
                cout<<"1: ";
                cin>>origin;
                cout<<endl;
                cout<<"2: ";
                cin>>destination;
                cout<<endl;
                bool originExists=false,destinationExists=false;
                for(unsigned int i=0;i<initNet.routing_table.size();i++){
                    if (initNet.routing_table[i].getName()==origin){
                        originExists=true;
                        break;
                    }
                }
                for(unsigned int i=0;i<initNet.routing_table.size();i++){
                    if (initNet.routing_table[i].getName()==destination){
                        destinationExists=true;
                        break;
                    }
                }
                cout<<"Enter cost of the connection (integer numbers only): ";
                cin>>cost;
                for(unsigned int i=0;i<initNet.routing_table.size();i++){ //se le asigna la conexion al router origen
                    if (initNet.routing_table[i].getName()==origin && destinationExists){
                        initNet.routing_table[i].addLink(destination,cost);
                        cout<<"Network Updated..."<<endl;
                        cout<<endl;
                        break;
                    }
                    else if (!destinationExists){
                        cout<<"Destination Router does not exist."<<endl;
                        break;
                    }
                }

                for(unsigned int i=0;i<initNet.routing_table.size();i++){ //se le asigna la conexion al router origen
                    if (initNet.routing_table[i].getName()==destination && originExists){
                        initNet.routing_table[i].addLink(origin,cost);
                        cout<<"Network Updated..."<<endl;
                        cout<<endl;
                        break;
                    }
                    else if(!originExists){
                        cout<<"Origin Router does not exist."<<endl;
                        break;
                    }
                }
                origin="";
                destination="";
            }
            if (option1==0){

            }
            else if (option1!=0 && option1!=2 && option1!=1){
                cout<<"Enter a valid option."<<endl;
            }

        }
        if (option==2){
            int option2;
            cout<<"1. Remove Routers."<<endl;
            cout<<"2. Remove Links between routers."<<endl<<"0. Return."<<endl; //verificar en todo el menu que los return funcionen
            cout<<"Input: ";
            cin>>option2;
            cout<<endl;
            if (option2==1){
                string routerDelete;
                cout<<"Available Routers in the Network to delete:"<<endl;
                for (unsigned int i=0;i<initNet.routing_table.size();i++){
                    cout<<initNet.routing_table[i].getName()<<endl;
                }
                cout<<"Enter the name of the Router to be deleted: "<<endl;
                cout<<"1: ";
                cin>>routerDelete;
                //verifico con quien tiene conexiones y se eliminan
                for(unsigned int i=0;i<initNet.routing_table.size();i++){
                    if (initNet.routing_table[i].Links.find(routerDelete)!=initNet.routing_table[i].Links.end()){//si encontro en el router una conexion con el que va a borrar
                        initNet.routing_table[i].Links.erase(routerDelete);
                    }else {
                        cout<<"Router does not exist."<<endl;
                        break;
                    }
                }
                for(unsigned int i=0;i<initNet.routing_table.size();i++){  //elimina el router en si
                    if (initNet.routing_table[i].getName()==routerDelete){
                        initNet.deleteRouter(initNet.routing_table[i]);
                        cout<<"Network Updated..."<<endl;
                        cout<<endl;
                    }
                }

            }
            if (option2==2){
                string linkDelete1,linkDelete2;
                cout<<"Available Links in the Network to delete:"<<endl;
                for (unsigned int i=0;i<initNet.routing_table.size();i++){
                    cout<<initNet.routing_table[i].getName()<<" Linked to: ";
                    map <string,int>::iterator it;
                    for (it=initNet.routing_table[i].Links.begin();it!=initNet.routing_table[i].Links.end();it++){
                        cout<<it->first<<", ";
                    }
                    cout<<endl;
                }
                cout<<"Enter the name of the FIRST Router: "<<endl;
                cout<<"1: ";
                cin>>linkDelete1;
                cout<<"Enter the name of the SECOND Router: "<<endl;
                cout<<"1: ";
                cin>>linkDelete2;
                bool router1Exists=false,router2Exists=false;
                for (unsigned int i=0;i<initNet.routing_table.size();i++){
                    if(initNet.routing_table[i].getName()==linkDelete1){
                        router1Exists=true;
                        break;
                    }
                }
                for (unsigned int i=0;i<initNet.routing_table.size();i++){
                    if(initNet.routing_table[i].getName()==linkDelete2){
                        router2Exists=true;
                        break;
                    }
                }
                for(unsigned int i=0;i<initNet.routing_table.size();i++){
                    if (initNet.routing_table[i].getName()==linkDelete1 && router2Exists){
                        initNet.routing_table[i].removeLink(linkDelete2);
                        cout<<"Network Updated..."<<endl;
                        cout<<endl;
                    }
                    else if (!router2Exists){
                        cout<<"Second Router does not exist."<<endl;
                        break;
                    }
                }
                for(unsigned int i=0;i<initNet.routing_table.size();i++){
                    if (initNet.routing_table[i].getName()==linkDelete2 && router1Exists){
                        initNet.routing_table[i].removeLink(linkDelete1);
                        cout<<"Network Updated..."<<endl;
                        cout<<endl;
                    }
                    else if(!router1Exists){
                        cout<<"First Router does not exist."<<endl;
                        break;
                    }
                }
                linkDelete1="";
                linkDelete2="";
            }
            if (option2==0){

            }
            else if (option2!=0 && option2!=2 && option2!=1){
                cout<<"Enter a valid option."<<endl;
            }

        }
        if (option==5){
            ifstream fin;
            string fileName,row,routerName,linkNameCost,linkName;
            int cost;
            unsigned int cont=0;
            cout<<"Enter name of the file (.txt): ";
            cin>>fileName;
            try {
                fin.open(fileName);
                if (!fin.is_open()) throw '1';

                Network loadedNet;
                Router tempRouter;

                while(fin.good()){
                    getline(fin,row);

                    for(unsigned int i=0;i<row.size();i++){
                        if (row[i]!=';'){
                            routerName+=row[i];
                        }
                        else {
                            tempRouter.setName(routerName);
                            loadedNet.addRouter(tempRouter);
                            row.erase(0,i+1);
                            routerName="";
                            break;
                        }
                    }

                    for (unsigned int i=0;i<row.size();i++){
                        if (row[i]!=','){
                            linkNameCost+=row[i];
                        }
                        else{
                            unsigned int index=linkNameCost.find('-');
                            linkName=linkNameCost.substr(0,index);
                            cost=atoi(linkNameCost.substr(index+1).c_str());
                            loadedNet.routing_table.at(cont).addLink(linkName,cost);
                            linkName="";
                            linkNameCost="";
                        }
                    }
                    cont++;
                }
                initNet=loadedNet;
                cout<<"NETWORK has been loaded successfully..."<<endl;
                cout<<endl;
                fin.close();
            } catch (char e) {
                cout<<"Error ";
                if (e=='1') cout<<"1: File could not be opened. (Check name and add extension .txt)."<<endl<<endl;
            }
            catch (...){
                cout<<"Unidentified Error. Try again."<<endl<<endl;
            }
        }
        if (option==3){
            string origin="", destination="";
            cout<<"Available Routers and their links to check: "<<endl;
            for (unsigned int i=0;i<initNet.routing_table.size();i++){
                cout<<initNet.routing_table[i].getName()<<" Linked to: ";
                map <string,int>::iterator it;
                for (it=initNet.routing_table[i].Links.begin();it!=initNet.routing_table[i].Links.end();it++){
                    cout<<it->first<<", ";
                }
                cout<<endl;
            }
            cout<<"Enter origin Router: ";
            cin>>origin;
            cout<<endl;
            cout<<"Enter destination Router: ";
            cin>>destination;
            cout<<endl;
            cout<<"Cost to deliver through linkage: ";
            bool originExists=false,destinationExists=false;
            for (unsigned int i=0;i<initNet.routing_table.size();i++){
                if (initNet.routing_table[i].getName()==origin){
                    originExists=true;
                    break;
                }
            }
            for (unsigned int i=0;i<initNet.routing_table.size();i++){
                if (initNet.routing_table[i].getName()==destination){
                    destinationExists=true;
                    break;
                }
            }
            map <string,int> test;
            if (originExists && destinationExists){
                test= queryPath(initNet,origin);
                cout<<test[destination]<<endl;
                cout<<endl;
            }
            else if(!originExists) cout<<"Origin Router does not exist."<<endl;
            else if(!destinationExists) cout<<"Destination Router does not exist."<<endl;


        }
        if (option==4){
            string origin, destination;
            cout<<"Available Routers in the Network: "<<endl;
            for (unsigned int i=0;i<initNet.routing_table.size();i++){
                cout<<initNet.routing_table[i].getName()<<endl;
            }
            cout<<"Enter name of origin Router: ";
            cin>>origin;
            cout<<endl;
            cout<<"Enter name of destination Router: ";
            cin>>destination;
            cout<<endl;
            bool originExists=false,destinationExists=false;
            for (unsigned int i=0;i<initNet.routing_table.size();i++){
                if (initNet.routing_table[i].getName()==origin){
                    originExists=true;
                    break;
                }
            }
            for (unsigned int i=0;i<initNet.routing_table.size();i++){
                if (initNet.routing_table[i].getName()==destination){
                    destinationExists=true;
                    break;
                }
            }
            string finalPath;
            if(originExists && destinationExists){
                finalPath=path(initNet,origin,destination);
                cout<<"Best Path for delivery between routers is: ";
                for (unsigned int i=finalPath.size();i>0;i--){
                    cout<<finalPath[i];
                }
                cout<<endl<<"With a total cost of: ";
                map <string,int> test;
                test= queryPath(initNet,origin);
                cout<<test[destination]<<endl;
                cout<<endl;

            }
            else if(!originExists) cout<<"Origin Router does not exist."<<endl<<endl;
            else if(!destinationExists) cout<<"Destination Router does not exist."<<endl<<endl;
            cout<<endl;
        }
        if (option==6){
            cout<<"Changes won't be saved."<<endl;
            cout<<endl;
            break;
        }
        if(option==0){
            cin.clear();
            cin.ignore();
            cout<<"Enter a valid number: "<<endl<<endl;;
        }
    }
}
map <string,int> queryPath (Network net,string origin){
    map <string, int> distFomSRC;
    vector <Router> routers;
    map <string,int> visited;
    string path;
    Router temp;
    distFomSRC[origin]=0;
    for (unsigned int i=0; i<net.routing_table.size();i++){
        if(net.routing_table[i].getName()!=origin){
            distFomSRC.insert({net.routing_table[i].getName(),10000000000});
        }
        routers.push_back(net.routing_table[i]);
    }
    vector<Router>::iterator firstIt=routers.begin();
    for(firstIt;firstIt!=routers.end();firstIt++){
        if (firstIt->getName()==origin){
            temp=*firstIt;
        }
    }

    while (!routers.empty()){
        string minRouterCost;
        int minCost=1000000000;
        map <string,int>::iterator it;
        for(it=temp.Links.begin();it!=temp.Links.end() && it->first!=temp.getName();it++){ //REVISAr
            if(it->second+distFomSRC[temp.getName()]<distFomSRC[it->first] && visited.find(it->first)==visited.end()){
                distFomSRC[it->first]=it->second+distFomSRC[temp.getName()];
            }
        }
        visited.insert({temp.getName(),0});
        vector<Router>::iterator routIt=routers.begin();
        for (routIt;routIt!=routers.end();routIt++){
            if (routIt->getName()==temp.getName()){
                routers.erase(routIt);
                break;
            }
        }
        for(routIt=routers.begin();routIt!=routers.end();routIt++){
            if(distFomSRC[routIt->getName()]<minCost){
                minCost=distFomSRC[routIt->getName()];
                minRouterCost=routIt->getName();
            }
        }
        for(routIt=routers.begin();routIt!=routers.end();routIt++){
            if(routIt->getName()==minRouterCost){
                temp=*routIt;
            }
        }

    }

    return distFomSRC;
}

string path (Network net,string origin,string destination){
    map <string, int> distFomSRC;
    map <string,Router> previous;
    vector <Router> routers;
    map <string,int> visited;
    string path;
    Router temp;
    distFomSRC[origin]=0;
    for (unsigned int i=0; i<net.routing_table.size();i++){
        if(net.routing_table[i].getName()!=origin){
            distFomSRC.insert({net.routing_table[i].getName(),10000000000});
        }
        routers.push_back(net.routing_table[i]);
    }
    vector<Router>::iterator firstIt=routers.begin();
    for(firstIt;firstIt!=routers.end();firstIt++){
        if (firstIt->getName()==origin){
            temp=*firstIt;
        }
    }

    while (!routers.empty()){
        string minRouterCost;
        int minCost=1000000000;
        map <string,int>::iterator it;
        for(it=temp.Links.begin();it!=temp.Links.end() && it->first!=temp.getName();it++){ //REVISAr
            if(it->second+distFomSRC[temp.getName()]<distFomSRC[it->first] && visited.find(it->first)==visited.end()){
                distFomSRC[it->first]=it->second+distFomSRC[temp.getName()];
                previous[it->first]=temp;
            }
        }
        visited.insert({temp.getName(),0});
        vector<Router>::iterator routIt=routers.begin();
        for (routIt;routIt!=routers.end();routIt++){
            if (routIt->getName()==temp.getName()){
                routers.erase(routIt);
                break;
            }
        }
        if (temp.getName()==destination){
            break;
        }
        for(routIt=routers.begin();routIt!=routers.end();routIt++){
            if(distFomSRC[routIt->getName()]<minCost){
                minCost=distFomSRC[routIt->getName()];
                minRouterCost=routIt->getName();
            }
        }
        for(routIt=routers.begin();routIt!=routers.end();routIt++){
            if(routIt->getName()==minRouterCost){
                temp=*routIt;
            }
        }

    }
    while (temp.getName()!=""){
        path+=" "+temp.getName();
        temp=previous[temp.getName()];
    }
    return path;
}
