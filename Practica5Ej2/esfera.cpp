#include "esfera.h"


Esfera::Esfera(float r, float x_, float y_)
{
    radio=r;
    px=x_;
    py=y_;
}

QRectF Esfera::boundingRect() const{
    return QRectF(px,py,2*radio,2*radio);
}
void Esfera::paint(QPainter *painter,const QStyleOptionGraphicsItem *option,QWidget *widget){
    painter->setBrush(Qt::darkCyan);
    painter->drawEllipse(boundingRect());
}
