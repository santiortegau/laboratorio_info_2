#ifndef ESFERA_H
#define ESFERA_H
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsItem>


class Esfera: public QGraphicsItem
{
public:
    Esfera(float r, float x_,float y_);
    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
private:
    float radio;
    float px;
    float py;
};

#endif // ESFERA_H
