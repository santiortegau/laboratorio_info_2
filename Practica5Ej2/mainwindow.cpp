#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(this);
    scene->setSceneRect(0,0,400,400);
    ui->graphicsView->setScene(scene);

    esfera[0]= new Esfera(10,90,90);
    scene->addItem(esfera[0]);
    esfera[1]= new Esfera(20,280,80);
    scene->addItem(esfera[1]);
    esfera[2]= new Esfera(30,270,270);
    scene->addItem(esfera[2]);
    esfera[3]= new Esfera(40,60,260);
    scene->addItem(esfera[3]);
    esfera[4]= new Esfera(5,195,195);
    scene->addItem(esfera[4]);

}

MainWindow::~MainWindow()
{
    delete ui;
}

