#include <iostream>

using namespace std;

/* This program gets an integer (n) and returns the maximum prime divisor of that number.
 * REQUIREMENTS:
 *  -Integer number greater than zero.
 * GUARANTEES:
 *  -The return of the maximum prime divisor of the input number.
 */

int main()
{
    int max=0, n,cont=0;
    cout<<"Ingrese un numero: "<<endl;
    cin>>n;
    for (int i=n;i!=0;i--){
        if (n%i==0){                    //Tests each number less than the input number to see if it's divisor of n
            for (int j=1;j<=i;j++){     //This loop uses an if statement to find the divisors of each (i) number and increments cont if true.
                if (i%j==0) cont++;
            }
            if (cont<3){                //After finding its divisors if they are exactly 2, i is a prime number
                if (i>max) max=i;       //If the prime number found is greater than a previous one add it as max
                cont=0;
            }
            else cont=0;                //If the test number is not a prime number it won't be added as maximum divisor
        }

    }
    cout<<"El mayor factor primo de "<<n<<" es: "<<max<<endl;
}
