#ifndef TUTORIAL_H
#define TUTORIAL_H

#include <QWidget>
#include <qgraphicsscene.h>
#include <iostream>
using namespace std;

namespace Ui {
class tutorial;
}

class tutorial : public QWidget
{
    Q_OBJECT
public:
    explicit tutorial(QWidget *parent = nullptr);
    ~tutorial();

signals:
    void startgame();

private slots:
    void on_pushButton_clicked();

private:
    Ui::tutorial *ui;
    QGraphicsScene *scene;
};

#endif // TUTORIAL_H
