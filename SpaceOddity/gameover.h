#ifndef GAMEOVER_H
#define GAMEOVER_H

#include <QWidget>
#include <QGraphicsScene>
#include <iostream>
using namespace std;

namespace Ui {
class gameover;
}

class gameover : public QWidget
{
    Q_OBJECT

public:
    explicit gameover(QWidget *parent = nullptr);
    ~gameover();
    void setTimeElapsed(double ms);
    void setLoser(string loser);

private slots:
    void on_pushButton_clicked();

private:
    Ui::gameover *ui;
    QGraphicsScene *scene;

};

#endif // GAMEOVER_H
