#include "planet.h"
#include "asteroid.h"
#include "cstdlib"
#include "time.h"

planetSim::planetSim(double px_, double py_, double rad_, double mass_)
{
    px=px_;
    py=py_;
    rad=rad_;
    mass=mass_;
}

double planetSim::getPx()
{
    return px;
}

double planetSim::getPy()
{
    return py;
}

double planetSim::getRad()
{
    return rad;
}

double planetSim::getMass()
{
    return mass;
}

void planetSim::setPos(double px_, double py_)
{
    px=px_;
    py=py_;
}

planet::planet(QGraphicsItem *planet): QGraphicsPixmapItem (planet)
{
    //randomly assign each planet object an image
    double randomMass;
    int randomPic;
    randomPic=rand()%11;
    switch (randomPic) {
    case 1:
        setPixmap(QPixmap(":/jupiter.png"));
        break;
    case 2:
        setPixmap(QPixmap(":/earth.png"));
        break;
    case 3:
        setPixmap(QPixmap(":/3.png"));
        break;
    case 4:
        setPixmap(QPixmap(":/4.png"));
        break;
    case 5:
        setPixmap(QPixmap(":/5.png"));
        break;
    case 6:
        setPixmap(QPixmap(":/6.png"));
        break;
    case 7:
        setPixmap(QPixmap(":/7.png"));
        break;
    case 8:
        setPixmap(QPixmap(":/8.png"));
        break;
    case 9:
        setPixmap(QPixmap(":/9.png"));
        break;
    case 10:
        setPixmap(QPixmap(":/10.png"));
        break;
    default:
        setPixmap(QPixmap(":/earth.png"));
        break;

    }
    //Rand() returns integer numbers, static cast allows them to be casted into double
    randomMass= 1000+static_cast<double>(rand())/(static_cast<double>(RAND_MAX/15000));
    planetSimulation= new planetSim(200,200,40,randomMass);
    position(this->getPlanet()->getPx(),this->getPlanet()->getPy());

}

planet::~planet()
{
    delete planetSimulation;
}

planetSim *planet::getPlanet()
{
    return planetSimulation;
}

void planet::position(double px_, double py_)
{
    this->getPlanet()->setPos(px_,py_);
    setPos(px_,py_);
}
