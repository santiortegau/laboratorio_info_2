#include "mainwindow.h"
#include "ui_mainwindow.h"
#define k 0.001
#define g 9.8

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    srand(time(NULL));
    scene = new QGraphicsScene(40,40,500,300);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/menu.png")));
    ui->registration->setStyleSheet("QCommandLinkButton {color: white;}");
    ui->loadplayer->setStyleSheet("QCommandLinkButton {color: white;}");
    ui->quitgame->setStyleSheet("QCommandLinkButton {color: white;}");
    backgroundMusic= new QMediaPlayer;
    backgroundMusic->setMedia(QUrl("qrc:/MainMusic.mp3"));
    backgroundMusic->setVolume(75);
    backgroundMusic->play();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
    delete backgroundMusic;
}

void MainWindow::on_registration_clicked()
{
    registration *regPage= new registration;
    regPage->show();

}

void MainWindow::on_quitgame_clicked()
{
    backgroundMusic->stop();
    this->close();

}

void MainWindow::on_loadplayer_clicked()
{
    loadgame *log= new loadgame;
    log->show();
}

void MainWindow::on_registration_2_clicked()
{
    credits *cred= new credits;
    cred->show();

}
