#-------------------------------------------------
#
# Project created by QtCreator 2018-11-28T21:20:31
#
#-------------------------------------------------

QT       += core gui serialport \
        multimedia


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpaceOddity
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 resources_big

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    asteroid.cpp \
    nebula.cpp \
    planet.cpp \
    spaceship.cpp \
    spaceport.cpp \
    ammo.cpp \
    maingame.cpp \
    registration.cpp \
    gameover.cpp \
    winner.cpp \
    loadgame.cpp \
    tutorial.cpp \
    credits.cpp \
    flag.cpp \
    powerups.cpp

HEADERS += \
        mainwindow.h \
    asteroid.h \
    nebula.h \
    planet.h \
    spaceship.h \
    spaceport.h \
    ammo.h \
    maingame.h \
    registration.h \
    gameover.h \
    winner.h \
    loadgame.h \
    tutorial.h \
    credits.h \
    flag.h \
    powerups.h

FORMS += \
        mainwindow.ui \
    maingame.ui \
    registration.ui \
    gameover.ui \
    winner.ui \
    loadgame.ui \
    tutorial.ui \
    credits.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    asteroid.qrc \
    asteroid.qrc

DISTFILES += \
    gameover.png
