#include "powerups.h"
#define g 10

powerups::powerups(QObject *parent) : QObject(parent)
{
    Px=100+ static_cast<double>(rand())/(static_cast<double>(RAND_MAX/1100));
    Py=0;
    Vx=0;
    Vy=80;
    setPixmap(QPixmap(":/logo.png"));
    this->setScale(0.1);
    powerTimer= new QTimer();
    connect(powerTimer,SIGNAL(timeout()),this,SLOT(movePower()));
    powerTimer->start(10);


}

powerups::~powerups()
{
    delete powerTimer;
}

void powerups::movePower()
{
    Vy+=g*0.01;
    Py+=Vy*0.01-(0.5*g*0.01*0.01);
    setPos(Px,Py);
    if(x()>1366){
        scene()->removeItem(this);
        delete this;
    }
    if(y()>676){
        scene()->removeItem(this);
        delete this;
    }

}
