#include "spaceport.h"
#include "cstdlib"
#include "time.h"
spaceport::spaceport(QGraphicsItem *spaceport): QGraphicsPixmapItem (spaceport)
{
    setPixmap(QPixmap(":/station.png"));
    randPos= 1+rand()%4;
    //asigns a random position to the station represented by the 4 boundaries of the screen
    if(randPos==1){
        this->setRotation(90);
        px=40;
        py=200+static_cast<double>(rand())/(static_cast<double>(RAND_MAX/450));
        this->setPos(px,py);
    }
    if(randPos==2){
        px=100+ static_cast<double>(rand())/(static_cast<double>(RAND_MAX/1000));
        py=636;
        this->setPos(px,py);
    }
    if(randPos==3){
        this->setRotation(270);
        px=1320;
        py=100+static_cast<double>(rand())/(static_cast<double>(RAND_MAX/450));
        this->setPos(px,py);
    }
    if(randPos==4){
        this->setRotation(180);
        px=200+ static_cast<double>(rand())/(static_cast<double>(RAND_MAX/1000));
        py=40;
        this->setPos(px,py);
    }
}

spaceport::~spaceport()
{
    delete this;
}

double spaceport::getPx()
{
    return px;
}

double spaceport::getPy()
{
    return py;
}

int spaceport::getRandPos()
{
    return randPos;
}
