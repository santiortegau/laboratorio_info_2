#include "tutorial.h"
#include "ui_tutorial.h"

tutorial::tutorial(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tutorial)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    scene= new QGraphicsScene(40,20,500,400);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/tutorial.png")));
}

tutorial::~tutorial()
{
    delete ui;
    delete scene;
}

void tutorial::on_pushButton_clicked()
{
    emit startgame();
    this->close();
}
