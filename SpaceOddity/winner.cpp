#include "winner.h"
#include "ui_winner.h"

winner::winner(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::winner)
{
    ui->setupUi(this);
    scene =new QGraphicsScene(40,40,300,200);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/winner2.png")));
}

winner::~winner()
{
    delete ui;
    delete scene;
}

void winner::setWinner(string player)
{
    ui->label->setText(QString::fromStdString(player));
    ui->label->setStyleSheet("QLabel {color: white;}");
}
