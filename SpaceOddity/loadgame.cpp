#include "loadgame.h"
#include "ui_loadgame.h"

loadgame::loadgame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::loadgame)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(40,40,500,300);
    string game,data;
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/menu.png")));
    //opens an input file just for reading operations
    fin.open("Spaceoddity.txt",ios::in);
    while(fin.good()){
        getline(fin,game);
        if(game[0]=='1') gameMode="Single Player";
        else if(game[0]=='2') gameMode= "Multi Player";
        game.erase(0,2);
        for(unsigned int i=0;i<game.size();i++){
            if(game[i]!=';'){
                player1+=game[i];
            }
            else{
                game.erase(0,i+1);
                break;
            }
        }
        for (unsigned int i=0;i<game.size();i++){
            if(game[i]!=';'){
                player2+=game[i];
            }
            else{
                game.erase(0,i+1);
                break;
            }
        }
        if(game[0]=='1') controller= "Keyboard";
        else if(game[0]=='2') controller= "Joystick";
        game.erase(0,2);
        if(game[0]=='1') level="Easy";
        else if(game[0]=='2') level="Medium";
        else if(game[0]=='3') level="Hard";
        data="Player 1: "+player1+". Player 2: "+player2+". Controller: "+controller+". Level: "+level;
        //data stores what will be displayed in the list widget
        ui->listWidget->addItem(QString::fromStdString(data));
        player1="";
        player2="";
        controller="";
        level="";
        gameMode="";
    }
}

loadgame::~loadgame()
{
    delete ui;
    delete scene;
    delete Main;
}

void loadgame::on_listWidget_itemSelectionChanged()
{
    //When user selects an item from the list, loading data grabs that information
    loadingData=ui->listWidget->currentItem()->text();

}

void loadgame::on_pushButton_clicked()
{
    loadingDatastr=loadingData.toStdString();
    if(loadingDatastr.size()!=0){
        loadingDatastr.erase(0,10);
        for(unsigned int i=0;i<loadingDatastr.size();i++){
            if(loadingDatastr[i]!='.'){
                player1Load+=loadingDatastr[i];
            }
            else{
                loadingDatastr.erase(0,i+2);
                break;

            }
        }
        loadingDatastr.erase(0,10);
        for(unsigned int i=0;i<loadingDatastr.size();i++){
            if(loadingDatastr[i]!='.'){
                if(loadingDatastr[i]==' '){
                    player2Load=" ";
                    loadingDatastr.erase(0,i+3);
                    break;
                }
                player2Load+=loadingDatastr[i];
            }
            else{
                gameModeLoad=2;
                loadingDatastr.erase(0,i+2);
                break;

            }
        }
        loadingDatastr.erase(0,12);
        if(loadingDatastr[0]=='K') controllerLoad=1;
        else if(loadingDatastr[0]=='J') {
            controllerLoad=2;
        }
        if (*(loadingDatastr.end()-1)=='y') levelLoad=1;
        else if(*(loadingDatastr.end()-1)=='m') levelLoad=2;
        else if(*(loadingDatastr.end()-1)=='d') levelLoad =3;
        //Generates a new game with the information selected on the widget
        Main= new mainGame(player1Load,player2Load,gameModeLoad,controllerLoad,levelLoad);
        Main->show();
        this->close();
        player1Load="";
        player2Load="";
    }
}
