#ifndef MAINGAME_H
#define MAINGAME_H

#include <QWidget>
#include <planet.h>
#include <asteroid.h>
#include <spaceport.h>
#include <spaceship.h>
#include <nebula.h>
#include <qgraphicsscene.h>
#include <iostream>
#include <gameover.h>
#include <QTime>
#include <winner.h>
#include <flag.h>
#include <powerups.h>

using namespace std;
namespace Ui {
class mainGame;
}

class mainGame : public QWidget
{
    Q_OBJECT

public:
    explicit mainGame(string player1, string player2, int gameMode, int controller, int level,QWidget *parent = nullptr);
    ~mainGame();
public slots:
    //slot  that controls nebulae appearing on screen
    void spawnNebula();
    void spawnPower();
    void alive();
    void timeLeft();
    void won();
    void crashed();
signals:
    void GO();

private:
    Ui::mainGame *ui;
    int players;
    QGraphicsScene *scene;
    QTimer *nebulaSpawnTimer;
    QTimer *powerupSpawnTimer;
    QTimer *alivetimer;
    QList <planet*> planets;
    QList <asteroid*> asteroids;
    QTime *gameTimer;
    int planetsVisited;
    ammo *laser;
    void closeEvent(QCloseEvent *event);


};

#endif // MAINGAME_H
