#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include <QFile>
#include <QtSerialPort/QSerialPort>
#include <QFileDialog>
#include <QDebug>
#include <QTextStream>
#include <QtSerialPort/QSerialPortInfo>
#include <QFile>
#include <registration.h>
#include <QtMultimedia/qmediaplayer.h>
#include <loadgame.h>
#include <credits.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_registration_clicked();

    void on_quitgame_clicked();

    void on_loadplayer_clicked();

    void on_registration_2_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QMediaPlayer *backgroundMusic;

};

#endif // MAINWINDOW_H
