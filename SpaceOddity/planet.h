#ifndef PLANET_H
#define PLANET_H
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>

class planetSim{
public:
    planetSim(double px_, double py_, double rad_,double mass_);
    double getPx();
    double getPy();
    double getRad();
    double getMass();
    void setPos(double px_,double py_);
private:
    double px;
    double py;
    double rad;
    double mass;

};


class planet: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    planet(QGraphicsItem* planet=0);
    ~planet();
    planetSim *getPlanet();
    void position(double px_,double py_);
private:
    planetSim* planetSimulation;


};

#endif // PLANET_H
