#include "asteroid.h"
#include <qmath.h>
#define G 1.00
#include <QDebug>
#include <cstdlib>
#include <planet.h>
#include <QPointF>
#include <QLineF>
#include <QTime>

asteroidSim::asteroidSim(){
    rad=20;
    R=3.0+static_cast<double>(rand())/(static_cast<double>(RAND_MAX/4.0));

}

void asteroidSim::setPos(double px_, double py_){
    px=px_;
    py=py_;

}

void asteroidSim::setRad(double rad_)
{
    rad=rad_;
}


void asteroidSim::update(planet &assignedPlanet)
{
    //Simulation of rotational movement
    px=assignedPlanet.getPlanet()->getPx()+assignedPlanet.getPlanet()->getRad()-(rad/2)+R*rad*qCos(angle);
    py=assignedPlanet.getPlanet()->getPy()+assignedPlanet.getPlanet()->getRad()-(rad/2)+R*rad*qSin(angle);


}

double asteroidSim::getPx()
{
    return px;
}

double asteroidSim::getPy()
{
    return py;
}

double asteroidSim::getRad()
{
    return rad;
}

asteroid::asteroid(QGraphicsItem *asteroid): QGraphicsPixmapItem (asteroid)
{
    setPixmap(QPixmap(":/bennu.png"));
    asteroidsimulation= new asteroidSim();
    asteroid_timer= new QTimer();
    asteroid_timer->start(5);
    connect(asteroid_timer,SIGNAL(timeout()),this,SLOT(moveAsteroid()));
}

asteroid::~asteroid()
{
    delete asteroidsimulation;
    delete asteroid_timer;
}

asteroidSim *asteroid::getAsteroid()
{
    return asteroidsimulation;
}
void asteroid::update(float dt)
{
    calcAccAsteroids(dt,*assignedPlanet);
    asteroidsimulation->update(*assignedPlanet);
    position(this->getAsteroid()->getPx(),this->getAsteroid()->getPy());
}

void asteroid::position(double px_,double py_)
{
    setPos(px_,py_);
}

void asteroid::calcAccAsteroids(float dt,planet &assignedPlanet)
{
    //Set the angle of the rotational movement using orbital speed
    this->getAsteroid()->angle+=(sqrt((G*assignedPlanet.getPlanet()->getMass())/(((assignedPlanet.getPlanet()->getPx()-this->getAsteroid()->getPx())*(assignedPlanet.getPlanet()->getPx()-this->getAsteroid()->getPx()))+((assignedPlanet.getPlanet()->getPy()-this->getAsteroid()->getPy())*(assignedPlanet.getPlanet()->getPy()-this->getAsteroid()->getPy())))))*dt;

}

void asteroid::destroyAsteroid()
{
    //Timer that destroys each asteroid with a single timeout signal
    setPixmap(QPixmap(":/explosion.png"));
    QTimer *destroy= new QTimer();
    destroy->singleShot(500,this,SLOT(removeDestroyedAst()));

}

void asteroid::moveAsteroid()
{
    this->update(0.01);

}

void asteroid::removeDestroyedAst()
{
    scene()->removeItem(this);
    delete this;
}

