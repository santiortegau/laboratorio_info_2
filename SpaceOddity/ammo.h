#ifndef AMMO_H
#define AMMO_H
#include <QObject>
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>
#include <planet.h>
#include "asteroid.h"
#include <QKeyEvent>
#include <QtMultimedia/qmediaplayer.h>


class ammo: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    ammo(QGraphicsItem *ammo=0);
    ~ammo();
    QTimer *bulletTimer;
    void setOrientation(int ori);
    int getOrientation();
public slots:
    void move();
private:
    int orientation;
    QMediaPlayer *ammosound;

};

#endif // AMMO_H
