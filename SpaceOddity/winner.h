#ifndef WINNER_H
#define WINNER_H

#include <QWidget>
#include <QGraphicsScene>
#include <iostream>
using namespace std;

namespace Ui {
class winner;
}

class winner : public QWidget
{
    Q_OBJECT

public:
    explicit winner(QWidget *parent = nullptr);
    ~winner();
    void setWinner(string player);

private:
    Ui::winner *ui;
    QGraphicsScene *scene;
};

#endif // WINNER_H
