#include "gameover.h"
#include "ui_gameover.h"

gameover::gameover(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::gameover)
{
    ui->setupUi(this);
    scene= new QGraphicsScene(40,40,300,200);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/gameover.png")));
}


gameover::~gameover()
{
    delete ui;
    delete scene;
}

void gameover::setTimeElapsed(double ms)
{
    //Displays time elapsed till gameover
    int i= ms/1000;
    QString num= QString::number(i);
    ui->lcdNumber->display(num);
}

void gameover::setLoser(string loser)
{

    ui->playername->setText(QString::fromStdString(loser));
    ui->playername->setStyleSheet("QLabel {color: white;}");

}


void gameover::on_pushButton_clicked()
{
    this->close();
}
