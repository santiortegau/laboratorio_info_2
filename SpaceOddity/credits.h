#ifndef CREDITS_H
#define CREDITS_H

#include <QWidget>
#include <qgraphicsscene.h>

namespace Ui {
class credits;
}

class credits : public QWidget
{
    Q_OBJECT

public:
    explicit credits(QWidget *parent = nullptr);
    ~credits();

private:
    Ui::credits *ui;
    QGraphicsScene *scene;
};

#endif // CREDITS_H
