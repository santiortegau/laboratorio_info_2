#include "maingame.h"
#include "ui_maingame.h"

mainGame::mainGame(string player1, string player2, int gameMode, int controller, int level,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mainGame)
{
    players=gameMode;
    //Just in case loadingdata from loadgame fails to catch information, set default values
    if(gameMode!=1 && gameMode!= 2)gameMode=1;
    if(controller!=2 && controller!=1) controller=1;
    if(level!=1&&level!=2&&level!=3) level =1;
    ui->setupUi(this);
    scene = new QGraphicsScene(35,40,1300,600);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/mainscene.png")));
    nebulaSpawnTimer = new QTimer;
    powerupSpawnTimer =new QTimer;
    gameTimer =new QTime;
    gameTimer->start();
    alivetimer= new QTimer;
    alivetimer->start();
    connect(nebulaSpawnTimer,SIGNAL(timeout()),this,SLOT(spawnNebula()));
    connect(powerupSpawnTimer,SIGNAL(timeout()),this,SLOT(spawnPower()));
    if (level==1){
        planet *planetTemp= new planet;
        planets.append(planetTemp);
        connect(alivetimer,SIGNAL(timeout()),this,SLOT(timeLeft()));
        planetTemp->position(650,300);
        scene->addItem(planetTemp);
        int randAst= 1+rand()%10;
        //generate random asteroids and assign a planet for them to orbit
        for (int i=0;i<randAst;i++){
            asteroid *ast= new asteroid;
            ast->assignedPlanet=planetTemp;
            ast->getAsteroid()->setPos(planetTemp->getPlanet()->getPx()+30,planetTemp->getPlanet()->getPy()+30);
            ast->position(ast->getAsteroid()->getPx()+30,ast->getAsteroid()->getPy()+30);
            scene->addItem(ast);
        }
        //Single Player mode
        if(gameMode==1){
            spaceship *spaceshipTemp= new spaceship(controller);
            spaceshipTemp->setName(player1);
            connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
            connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
            connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
            spaceshipTemp->planetsToVisit=planets.size();
            scene->addItem(spaceshipTemp);
            //Sets the focus to the spaceship so it can receive keyboard events
            spaceshipTemp->setFocus();
            spaceport *spacePortTemp= new spaceport;
            spaceshipTemp->startingPort=spacePortTemp;
            scene->addItem(spacePortTemp);
            //assigns the port to the spaceship
            spaceshipTemp->DockedPort=spacePortTemp;
            //orientates the spaceship according to the random position/orientation of the station
            spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
            //Centers the spaceship in the station
            if(spaceshipTemp->getOrientation()==1){
                spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
            }
            if(spaceshipTemp->getOrientation()==2){
                spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
            }
            if(spaceshipTemp->getOrientation()==3){
                spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
            }
            if(spaceshipTemp->getOrientation()==4){
                spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
            }
            spaceport *spacePortTemp2= new spaceport;
            if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                    if(spacePortTemp->getPy()>600){
                        spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                    }else{
                        spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                    }
                }
            }
            if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                    if(spacePortTemp->getPx()>800){
                        spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                    }else{
                        spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                    }
                }
            }
            scene->addItem(spacePortTemp2);
        }
        if (gameMode==2){
            for (int i =0;i<2;i++){
                if (i==0){
                    spaceship *spaceshipTemp= new spaceship(1);
                    spaceshipTemp->setName(player1);
                    spaceshipTemp->planetsToVisit=planets.size();
                    connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
                    connect(spaceshipTemp,SIGNAL(crashed()),this,SLOT(crashed()));
                    connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
                    connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
                    spaceshipTemp->setPixmap(QPixmap(":/spacex1.png"));
                    scene->addItem(spaceshipTemp);
                    spaceshipTemp->setFocus();
                    spaceport *spacePortTemp= new spaceport;
                    scene->addItem(spacePortTemp);
                    spaceshipTemp->DockedPort=spacePortTemp;
                    spaceshipTemp->startingPort=spacePortTemp;
                    spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
                    if(spaceshipTemp->getOrientation()==1){
                        spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
                    }
                    if(spaceshipTemp->getOrientation()==2){
                        spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
                    }
                    if(spaceshipTemp->getOrientation()==3){
                        spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
                    }
                    if(spaceshipTemp->getOrientation()==4){
                        spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
                    }
                    spaceport *spacePortTemp2= new spaceport;
                    if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                        if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                            if(spacePortTemp->getPy()>600){
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                            }
                        }
                    }
                    if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                        if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                            if(spacePortTemp->getPx()>1100){
                                spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                            }
                        }
                    }
                    scene->addItem(spacePortTemp2);
                }
                if (i==1){
                    spaceship *spaceshipTemp= new spaceship(2);
                    spaceshipTemp->setName(player2);
                    spaceshipTemp->planetsToVisit=planets.size();
                    connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
                    connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
                    connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
                    connect(spaceshipTemp,SIGNAL(crashed()),this,SLOT(crashed()));
                    scene->addItem(spaceshipTemp);
                    spaceport *spacePortTemp= new spaceport;
                    scene->addItem(spacePortTemp);
                    spaceshipTemp->DockedPort=spacePortTemp;
                    spaceshipTemp->startingPort=spacePortTemp;
                    spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
                    if(spaceshipTemp->getOrientation()==1){
                        spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
                    }
                    if(spaceshipTemp->getOrientation()==2){
                        spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
                    }
                    if(spaceshipTemp->getOrientation()==3){
                        spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
                    }
                    if(spaceshipTemp->getOrientation()==4){
                        spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
                    }
                    spaceport *spacePortTemp2= new spaceport;
                    if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                        if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                            if(spacePortTemp->getPy()>600){
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                            }
                        }
                    }
                    if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                        if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                            if(spacePortTemp->getPx()>1100){
                                spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                            }
                        }
                    }
                    scene->addItem(spacePortTemp2);
                }
            }
        }
        nebulaSpawnTimer->start(2000);
        powerupSpawnTimer->start(5000);
    }
    if (level==2){
        connect(alivetimer,SIGNAL(timeout()),this,SLOT(timeLeft()));
        for (int i =0;i<3;i++){
            planet *planetTemp= new planet;
            if (i==0){
                planetTemp->position(200,100);
            }
            if(i==1){
                planetTemp->position(650,300);

            }
            if(i==2){
                planetTemp->position(1100,400);
            }
            scene->addItem(planetTemp);
            planets.append(planetTemp);
        }
        int randAst= 1+rand()%10;
        for (int i=0;i<planets.size();i++){
            for(int j=0;j<randAst;j++){
                asteroid *ast= new asteroid;
                ast->assignedPlanet=planets.at(i);
                ast->getAsteroid()->setPos(planets.at(i)->getPlanet()->getPx()+30,planets.at(i)->getPlanet()->getPy()+30);
                ast->position(ast->getAsteroid()->getPx()+30,ast->getAsteroid()->getPy()+30);
                scene->addItem(ast);
            }
        }
        if (gameMode==1){
            spaceship *spaceshipTemp= new spaceship(controller);
            spaceshipTemp->setName(player1);
            spaceshipTemp->planetsToVisit=planets.size();
            connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
            connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
            connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
            scene->addItem(spaceshipTemp);
            spaceshipTemp->setFocus();
            spaceport *spacePortTemp= new spaceport;
            scene->addItem(spacePortTemp);
            spaceshipTemp->DockedPort=spacePortTemp;
            spaceshipTemp->startingPort=spacePortTemp;
            spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
            if(spaceshipTemp->getOrientation()==1){
                spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
            }
            if(spaceshipTemp->getOrientation()==2){
                spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
            }
            if(spaceshipTemp->getOrientation()==3){
                spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
            }
            if(spaceshipTemp->getOrientation()==4){
                spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
            }
            spaceport *spacePortTemp2= new spaceport;
            if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                    if(spacePortTemp->getPy()>600){
                        spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                    }else{
                        spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                    }
                }
            }
            if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                    if(spacePortTemp->getPx()>1100){
                        spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                    }else{
                        spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                    }
                }
            }
            scene->addItem(spacePortTemp2);
        }
        if (gameMode==2){
            for (int i =0;i<2;i++){
                if (i==0){
                    spaceship *spaceshipTemp= new spaceship(1);
                    spaceshipTemp->setName(player1);
                    spaceshipTemp->planetsToVisit=planets.size();
                    connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
                    connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
                    connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
                    connect(spaceshipTemp,SIGNAL(crashed()),this,SLOT(crashed()));
                    spaceshipTemp->setPixmap(QPixmap(":/spacex1.png"));
                    scene->addItem(spaceshipTemp);
                    spaceshipTemp->setFocus();
                    spaceport *spacePortTemp= new spaceport;
                    scene->addItem(spacePortTemp);
                    spaceshipTemp->DockedPort=spacePortTemp;
                    spaceshipTemp->startingPort=spacePortTemp;
                    spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
                    if(spaceshipTemp->getOrientation()==1){
                        spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
                    }
                    if(spaceshipTemp->getOrientation()==2){
                        spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
                    }
                    if(spaceshipTemp->getOrientation()==3){
                        spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
                    }
                    if(spaceshipTemp->getOrientation()==4){
                        spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
                    }
                    spaceport *spacePortTemp2= new spaceport;
                    if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                        if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                            if(spacePortTemp->getPy()>600){
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                            }
                        }
                    }
                    if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                        if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                            if(spacePortTemp->getPx()>1100){
                                spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                            }
                        }
                    }
                    scene->addItem(spacePortTemp2);
                }
                if (i==1){
                    spaceship *spaceshipTemp= new spaceship(2);
                    spaceshipTemp->setName(player2);
                    spaceshipTemp->planetsToVisit=planets.size();
                    connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
                    connect(spaceshipTemp,SIGNAL(crashed()),this,SLOT(crashed()));
                    connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
                    connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
                    scene->addItem(spaceshipTemp);
                    spaceport *spacePortTemp= new spaceport;
                    scene->addItem(spacePortTemp);
                    spaceshipTemp->DockedPort=spacePortTemp;
                    spaceshipTemp->startingPort=spacePortTemp;
                    spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
                    if(spaceshipTemp->getOrientation()==1){
                        spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
                    }
                    if(spaceshipTemp->getOrientation()==2){
                        spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
                    }
                    if(spaceshipTemp->getOrientation()==3){
                        spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
                    }
                    if(spaceshipTemp->getOrientation()==4){
                        spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
                    }
                    spaceport *spacePortTemp2= new spaceport;
                    if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                        if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                            if(spacePortTemp->getPy()>600){
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                            }
                        }
                    }
                    if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                        if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                            if(spacePortTemp->getPx()>1100){
                                spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                            }
                        }
                    }
                    scene->addItem(spacePortTemp2);
                }
            }

        }
        nebulaSpawnTimer->start(1800);
        powerupSpawnTimer->start(3000);
    }
    if (level==3){
        connect(alivetimer,SIGNAL(timeout()),this,SLOT(timeLeft()));
        for (int i =0;i<6;i++){
            planet *planetTemp= new planet;
            if(i==0){
                planetTemp->position(200,150);
            }
            if(i==1){
                planetTemp->position(650,250);
            }
            if(i==2){
                planetTemp->position(1100,450);
            }
            if(i==3){
                planetTemp->position(1100,150);
            }
            if(i==4){
                planetTemp->position(200,450);
            }
            if(i==5){
                planetTemp->position(650,450);
            }
            scene->addItem(planetTemp);
            planets.append(planetTemp);
        }
        int randAst= 1+rand()%4;
        for (int i=0;i<planets.size();i++){
            for(int j=0;j<randAst;j++){
                asteroid *ast= new asteroid;
                ast->assignedPlanet=planets.at(i);
                ast->getAsteroid()->setPos(planets.at(i)->getPlanet()->getPx()+30,planets.at(i)->getPlanet()->getPy()+30);
                ast->position(ast->getAsteroid()->getPx()+30,ast->getAsteroid()->getPy()+30);
                scene->addItem(ast);
            }
        }
        if(gameMode==1){
            spaceship *spaceshipTemp= new spaceship(controller);
            spaceshipTemp->setName(player1);
            spaceshipTemp->planetsToVisit=planets.size();
            connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
            connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
            connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
            scene->addItem(spaceshipTemp);
            spaceshipTemp->setFocus();
            spaceport *spacePortTemp= new spaceport;
            scene->addItem(spacePortTemp);
            spaceshipTemp->DockedPort=spacePortTemp;
            spaceshipTemp->startingPort=spacePortTemp;
            spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
            if(spaceshipTemp->getOrientation()==1){
                spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
            }
            if(spaceshipTemp->getOrientation()==2){
                spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
            }
            if(spaceshipTemp->getOrientation()==3){
                spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
            }
            if(spaceshipTemp->getOrientation()==4){
                spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
            }
            spaceport *spacePortTemp2= new spaceport;
            if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                    if(spacePortTemp->getPy()>600){
                        spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                    }else{
                        spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                    }
                }
            }
            if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                    if(spacePortTemp->getPx()>1100){
                        spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                    }else{
                        spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                    }
                }
            }
            scene->addItem(spacePortTemp2);
        }
        if (gameMode==2){
            for (int i =0;i<2;i++){
                if (i==0){
                    spaceship *spaceshipTemp= new spaceship(1);
                    spaceshipTemp->setName(player1);
                    spaceshipTemp->planetsToVisit=planets.size();
                    connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
                    connect(spaceshipTemp,SIGNAL(crashed()),this,SLOT(crashed()));
                    connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
                    connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
                    spaceshipTemp->setPixmap(QPixmap(":/spacex1.png"));
                    scene->addItem(spaceshipTemp);
                    spaceshipTemp->setFocus();
                    spaceport *spacePortTemp= new spaceport;
                    scene->addItem(spacePortTemp);
                    spaceshipTemp->DockedPort=spacePortTemp;
                    spaceshipTemp->startingPort=spacePortTemp;
                    spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
                    if(spaceshipTemp->getOrientation()==1){
                        spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
                    }
                    if(spaceshipTemp->getOrientation()==2){
                        spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
                    }
                    if(spaceshipTemp->getOrientation()==3){
                        spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
                    }
                    if(spaceshipTemp->getOrientation()==4){
                        spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
                    }
                    spaceport *spacePortTemp2= new spaceport;
                    if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                        if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                            if(spacePortTemp->getPy()>600){
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                            }
                        }
                    }
                    if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                        if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                            if(spacePortTemp->getPx()>1100){
                                spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                            }
                        }
                    }
                    scene->addItem(spacePortTemp2);
                }
                if (i==1){
                    spaceship *spaceshipTemp= new spaceship(2);
                    spaceshipTemp->setName(player2);
                    spaceshipTemp->planetsToVisit=planets.size();
                    connect(spaceshipTemp,SIGNAL(gameover()),this,SLOT(alive()));
                    connect(spaceshipTemp,SIGNAL(crashed()),this,SLOT(crashed()));
                    connect(spaceshipTemp,SIGNAL(won()),this,SLOT(won()));
                    connect(this,SIGNAL(GO()),spaceshipTemp,SLOT(GOver()));
                    scene->addItem(spaceshipTemp);
                    spaceport *spacePortTemp= new spaceport;
                    scene->addItem(spacePortTemp);
                    spaceshipTemp->DockedPort=spacePortTemp;
                    spaceshipTemp->startingPort=spacePortTemp;
                    spaceshipTemp->setOrientation(spacePortTemp->getRandPos());
                    if(spaceshipTemp->getOrientation()==1){
                        spaceshipTemp->position(spacePortTemp->getPx()+50,spacePortTemp->getPy()+20);
                    }
                    if(spaceshipTemp->getOrientation()==2){
                        spaceshipTemp->position(spacePortTemp->getPx()+25,spacePortTemp->getPy()-50);
                    }
                    if(spaceshipTemp->getOrientation()==3){
                        spaceshipTemp->position(spacePortTemp->getPx()-50,spacePortTemp->getPy()-25);
                    }
                    if(spaceshipTemp->getOrientation()==4){
                        spaceshipTemp->position(spacePortTemp->getPx()-25,spacePortTemp->getPy()+50);
                    }
                    spaceport *spacePortTemp2= new spaceport;
                    if(spacePortTemp->getRandPos()==1 || spacePortTemp->getRandPos()==3){
                        if(spacePortTemp->getPy()-spacePortTemp2->getPy()<90){
                            if(spacePortTemp->getPy()>600){
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()-90);
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x(),spacePortTemp2->y()+90);
                            }
                        }
                    }
                    if(spacePortTemp->getRandPos()==2 || spacePortTemp->getRandPos()==4){
                        if(spacePortTemp->getPx()-spacePortTemp2->getPx()<90){
                            if(spacePortTemp->getPx()>1100){
                                spacePortTemp2->setPos(spacePortTemp2->x()-90,spacePortTemp2->y());
                            }else{
                                spacePortTemp2->setPos(spacePortTemp2->x()+90,spacePortTemp2->y());
                            }
                        }
                    }
                    scene->addItem(spacePortTemp2);
                }
            }
        }
        nebulaSpawnTimer->start(1800);
        powerupSpawnTimer->start(3000);
    }
}

mainGame::~mainGame()
{
    delete ui;
    delete nebulaSpawnTimer;
    delete alivetimer;
    delete gameTimer;
    delete scene;
    delete powerupSpawnTimer;
}

void mainGame::spawnNebula()
{
    nebula *nebulaTemp= new nebula;
    scene->addItem(nebulaTemp);
}

void mainGame::spawnPower()
{
    powerups *powerTemp=new powerups();
    scene->addItem(powerTemp);
}

void mainGame::alive()
{
    if(players==1){
        //Whenever the spaceship emits a gameover signal this slot generates a new gameover object
        gameover *go= new gameover;
        go->show();
        spaceship *temp= dynamic_cast<spaceship*>(sender());
        if (temp){
            go->setLoser(temp->getName());
            temp->spaceshipDockingTimer->stop();
            temp->spaceshipTimer->stop();
            temp->orbitTimer->stop();
        }
        go->setTimeElapsed(gameTimer->elapsed());
        this->close();
        this->nebulaSpawnTimer->stop();
        this->alivetimer->stop();
        this->gameTimer->~QTime();
        emit GO();
    }
    if(players==2){
        spaceship *temp= dynamic_cast<spaceship*>(sender());
        if (temp){
            temp->spaceshipDockingTimer->stop();
            temp->spaceshipTimer->stop();
            temp->orbitTimer->stop();
            players=1;
        }
    }
}

void mainGame::timeLeft()
{
    int timeLeft=45;
        //If gametimer reaches 0, timeleft generates a new gameover object
        ui->lcdNumber->display(timeLeft-gameTimer->elapsed()/1000);
        if(timeLeft-gameTimer->elapsed()/1000==0 && alivetimer->isActive()){
            alivetimer->stop();
            gameover *go= new gameover;
            emit GO();
            go->show();
            go->setTimeElapsed(gameTimer->elapsed());
            this->close();
         }
}

void mainGame::won()
{
    //If the spaceship emits a winning signal this slot executes and generates a new winning object
    winner *win= new winner;
    spaceship *temp= dynamic_cast<spaceship*>(sender());
    if (temp){
        win->setWinner(temp->getName());
        temp->spaceshipDockingTimer->stop();
        temp->spaceshipTimer->stop();
        temp->orbitTimer->stop();
        temp->GOver();
    }
    win->show();
    this->close();


}

void mainGame::crashed()
{
    gameover *go= new gameover;
    go->show();
    spaceship *temp= dynamic_cast<spaceship*>(sender());
    if (temp){
        go->setLoser("Both Players");
        temp->spaceshipDockingTimer->stop();
        temp->spaceshipTimer->stop();
        temp->orbitTimer->stop();
        emit GO();
    }
    go->setTimeElapsed(gameTimer->elapsed());
    this->close();
    this->nebulaSpawnTimer->stop();
    this->alivetimer->stop();
    this->gameTimer->~QTime();

}

void mainGame::closeEvent(QCloseEvent *event)
{
    //Stop movements and timers whenever the user closes the screen
    event->accept();
    this->nebulaSpawnTimer->stop();
    this->alivetimer->stop();
    this->gameTimer->~QTime();
    QWidget::closeEvent(event);
    emit GO();


}
