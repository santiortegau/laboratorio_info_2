#include "nebula.h"
#include "math.h"
#define g -10

void nebula::moveNebula()
{
    //This slot simulates projectile motion using a gravity constant of -10m/s^2
    Vx=randVx*cos(atan2(Vy,Vx));
    Vy=randVx*sin(atan2(Vy,Vx))-g*0.01;
    Px+=Vx*0.01;
    Py+=50*sin(atan2(Vy,Vx))*0.01-(0.5*g*0.01*0.01);
    setPos(Px,Py);
    if(x()>1366){
        scene()->removeItem(this);
        delete this;
    }
    if(y()>676){
        scene()->removeItem(this);
        delete this;
    }

}

nebula::nebula(QGraphicsItem *nebula): QGraphicsPixmapItem (nebula)
{
    //Generates nebulae on the top left portion of the screen
    Px=-80;
    Py=0;
    Vx=50;
    Vy=50;
    randVx=50+rand()%350;
    setPixmap(QPixmap(":/nebula.png"));
    double randomPy= rand()%400;
    setPos(-80,randomPy);
    nebulaTimer= new QTimer();
    connect(nebulaTimer,SIGNAL(timeout()),this,SLOT(moveNebula()));
    nebulaTimer->start(10);

}

nebula::~nebula()
{
    delete nebulaTimer;
}
