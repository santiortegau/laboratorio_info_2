#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>
#include <QTimer>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSplashScreen *Logo=new QSplashScreen;
    Logo->setPixmap(QPixmap(":/logo.png"));
    Logo->show();
    MainWindow w;
    //after 3sec close the logo screen and show the mainwindow
    QTimer::singleShot(3000,Logo,SLOT(close()));
    QTimer::singleShot(3000,&w,SLOT(show()));
    w.show();
    return a.exec();
}
