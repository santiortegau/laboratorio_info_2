#ifndef SPACEPORT_H
#define SPACEPORT_H
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>

class spaceport: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    spaceport(QGraphicsItem *spaceport=0);
    ~spaceport();
    double getPx();
    double getPy();
    int getRandPos();
private:
    double px;
    double py;
    int randPos;


};

#endif // SPACEPORT_H
