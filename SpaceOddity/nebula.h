#ifndef NEBULA_H
#define NEBULA_H
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>


class nebula: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public slots:
    void moveNebula();
public:

    nebula(QGraphicsItem* nebula=0);
    ~nebula();
    QTimer *nebulaTimer;
    double Px,Py,Vx,Vy;
    double randVx;

};

#endif // NEBULA_H
