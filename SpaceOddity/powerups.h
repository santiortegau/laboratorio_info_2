#ifndef POWERUPS_H
#define POWERUPS_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>

class powerups : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit powerups(QObject *parent = nullptr);
    ~powerups();
    QTimer *powerTimer;
    double Px, Py, Vx, Vy;
public slots:
    void movePower();

signals:

public slots:
};

#endif // POWERUPS_H
