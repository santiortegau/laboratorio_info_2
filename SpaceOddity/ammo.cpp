#include "ammo.h"
#include <qmath.h>

ammo::ammo(QGraphicsItem *ammo): QGraphicsPixmapItem (ammo)
{
    //Create graphic content of the object
    setPixmap(QPixmap(":/laser.png"));
    pixmap().rect().setRect(0,0,7,70);
    bulletTimer= new QTimer();
    connect(bulletTimer,SIGNAL(timeout()),this,SLOT(move()));
    bulletTimer->start(10);
    ammosound= new QMediaPlayer;
    ammosound->setMedia(QUrl("qrc:/lasersound.WAV"));
    ammosound->setVolume(10);
    ammosound->play();

}

ammo::~ammo()
{
    delete bulletTimer;
    delete ammosound;
}

//Method used to orientate bullets according to spaceship orientation
void ammo::setOrientation(int ori)
{
    orientation=ori;
    if(orientation==1){
        setRotation(90);
    }
    if(orientation==3){
        setRotation(270);
    }
    if(orientation==4){
        setRotation(180);
    }
}

int ammo::getOrientation()
{
    return orientation;
}

void ammo::move()
{
    //dx and dy set up a movement step of 10px and moves the bullet according to its rotation
    double dx,dy;
    dx=10*qSin(qDegreesToRadians(rotation()));
    dy=10*qCos(qDegreesToRadians(rotation()));
    setPos(x()+dx,y()-dy);
    //When bullets reach any boundary of the main widget they're deleted
    if(pos().x()>1366){
        scene()->removeItem(this);
        delete this;
        return;
    }
    if(pos().y()<0){
        scene()->removeItem(this);
        delete this;
        return;
    }
    if(pos().x()<0){
        scene()->removeItem(this);
        delete this;
        return;
    }
    if(pos().y()>676){
        scene()->removeItem(this);
        delete this;
        return;
    }
    //loop that constantly checks if the bullet collides with an asteroid
    QList<QGraphicsItem*> colliding_items= collidingItems(); //returns a list of QGraphicsItem colliding with the objct
    for (int i=0;i<colliding_items.size();i++){
        //Tries to cast the GraphicsItem into an asteroid*
        asteroid *ast=dynamic_cast<asteroid*>(colliding_items[i]);
        //If effectively casted, destroy the asteroid
        if (ast){
            ast->destroyAsteroid();
            scene()->removeItem(this);
            delete this;
            return; //se sale de la funcion para que no ejecute la siguiente parte
        }
    }

}
