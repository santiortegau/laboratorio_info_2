#ifndef LOADGAME_H
#define LOADGAME_H

#include <QWidget>
#include <qgraphicsscene.h>
#include <fstream>
#include <iostream>
using namespace std;
#include <maingame.h>

namespace Ui {
class loadgame;
}

class loadgame : public QWidget
{
    Q_OBJECT

public:
    explicit loadgame(QWidget *parent = nullptr);
    ~loadgame();

private slots:
    void on_listWidget_itemSelectionChanged();

    void on_pushButton_clicked();

private:
    Ui::loadgame *ui;
    QGraphicsScene *scene;
    string player1, player2, gameMode, controller, level,loadingDatastr;
    ifstream fin;
    mainGame *Main;
    QString loadingData;
    string player1Load,player2Load;
    int gameModeLoad,controllerLoad,levelLoad;
    fstream fout;

};

#endif // LOADGAME_H
