#include "registration.h"
#include "ui_registration.h"


registration::registration(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::registration)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    scene = new QGraphicsScene(40,40,500,300);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/menu.png")));
    fout.open("Spaceoddity.txt",ios::app);

}

registration::~registration()
{
    delete ui;
    delete scene;
}

void registration::on_pushButton_6_clicked()
{
    gameMode=2;
    controller=2;
    this->ui->pushButton_6->setDisabled(true);
    ui->pushButton_7->setDisabled(true);
    ui->lineEdit_2->setEnabled(true);
    ui->pushButton_4->setDisabled(true);
    ui->pushButton_5->setDisabled(true);
}

void registration::on_pushButton_7_clicked()
{
    gameMode=1;
    ui->pushButton_7->setDisabled(true);
    ui->pushButton_6->setDisabled(true);
}


void registration::on_lineEdit_textEdited(const QString &arg1)
{
    player1=arg1;
}

void registration::on_lineEdit_2_textEdited(const QString &arg1)
{
    player2=arg1;
}

void registration::on_pushButton_4_clicked()
{
    controller=1;
    ui->pushButton_4->setDisabled(true);
    ui->pushButton_5->setDisabled(true);
}

void registration::on_pushButton_5_clicked()
{
    controller=2;
    ui->pushButton_5->setDisabled(true);
    ui->pushButton_4->setDisabled(true);
}

void registration::on_pushButton_clicked()
{
    level=1;
    ui->pushButton->setDisabled(true);
    ui->pushButton_2->setDisabled(true);
    ui->pushButton_3->setDisabled(true);
}

void registration::on_pushButton_2_clicked()
{
    level=2;
    ui->pushButton->setDisabled(true);
    ui->pushButton_2->setDisabled(true);
    ui->pushButton_3->setDisabled(true);
}

void registration::on_pushButton_3_clicked()
{
    level=3;
    ui->pushButton->setDisabled(true);
    ui->pushButton_2->setDisabled(true);
    ui->pushButton_3->setDisabled(true);
}

void registration::on_pushButton_8_clicked()
{
    //when the start game pushbutton is clicked generate a tutorial object and show it
    if(!ui->pushButton->isEnabled() && !ui->pushButton_2->isEnabled() && !ui->pushButton_3->isEnabled()
            && !ui->pushButton_4->isEnabled() && !ui->pushButton_5->isEnabled() && !ui->pushButton_6->isEnabled()
            && !ui->pushButton_7->isEnabled()){
        ui->pushButton_8->setDisabled(true);
        if(player2.size()==0) player2=" ";
        fout << gameMode <<";" << player1.toStdString()<<";"<<player2.toStdString()<<";"<<controller<<";"<<level<<endl;
        tutorial *tut= new tutorial;
        tut->show();
        //tutorial object emits a signal to start a new game with the information collected in this class
        connect(tut,SIGNAL(startgame()),this,SLOT(startGame()));
        fout.close();
    }

}

void registration::startGame()
{
    mainGame *Main= new mainGame(player1.toStdString(),player2.toStdString(),gameMode,controller,level);
    Main->show();
    this->close();

}
void registration::closeEvent(QCloseEvent *event)
{
    event->accept();
    QWidget::closeEvent(event);
}
