#ifndef SPACESHIP_H
#define SPACESHIP_H

#include <QObject>
#include <QtMultimedia/qmediaplayer.h>
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>
#include <planet.h>
#include "asteroid.h"
#include <QKeyEvent>
#include <ammo.h>
#include <spaceport.h>
#include <nebula.h>
#include <QtSerialPort/QSerialPort>
#include <QFile>
#include <QDebug>
#include <iostream>
#include <flag.h>
#include <powerups.h>
using namespace std;
class spaceshipSim
{
public:
    spaceshipSim();
    void setPos(double px_, double py_);
    void update(float dt);
    double getPx();
    double getPy();
    double getVx();
    double getVy();
    double getAx();
    double getAy();
    double getMass();
    double ax;
    double ay;
    void setVel(double vx_,double vy_);
private:
    double px;
    double py;
    double vx;
    double vy;
    double mass;

};

class spaceship: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
signals:
    void gameover();
    void won();
    void crashed();
public slots:
    void moveSpaceship();
    void moveSpaceshipJoystick();
    void dock();
    void orbit();
    void orbitmove();
    void GOver();
    void boostVel();
public:
    spaceship(int controller,QGraphicsItem* spaceship=0);
    ~spaceship();
    spaceshipSim * getSpaceship();
    void update(float dt);
    void position(double px_,double py_);
    double Vx=2;
    double Vy=2;
    QTimer *controlTimer;
    QTimer * spaceshipTimer;
    QTimer * spaceshipDockingTimer;
    QTimer * orbitTimer;
    void calcAccSpaceship(double dt, planet * orbitingPlanet);
    planet * orbitingPlanet;
    QList<planet*> planetsVisited;
    void setOrientation(int ori);
    int getOrientation();
    void keyPressEvent(QKeyEvent *event);
    ammo *laser;
    spaceport *DockedPort;
    spaceport *startingPort;
    double orbitalAngle;
    int planetsToVisit;
    string getName();
    void setName(string name_);
    QMediaPlayer *movingSound;
    QSerialPort *control;
    int contr;
private:

    bool disableKeys=false;
    spaceshipSim *spaceshipSimulation;
    int orientation;
    string name;



};

#endif // SPACESHIP_H
