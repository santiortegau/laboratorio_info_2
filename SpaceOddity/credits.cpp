#include "credits.h"
#include "ui_credits.h"

credits::credits(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::credits)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    scene=new QGraphicsScene(40,45,400,500);
    ui->graphicsView->setScene(scene);
    scene->setBackgroundBrush(QBrush(QImage(":/creditos.png")));
}

credits::~credits()
{
    delete ui;
    delete scene;
}
