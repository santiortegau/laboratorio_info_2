#ifndef ASTEROID_H
#define ASTEROID_H
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>
#include "planet.h"

class asteroidSim
{
public:
    asteroidSim();
    void setPos(double px_, double py_);
    void setRad(double rad_);
    void update(planet &assignedPlanet);
    double getPx();
    double getPy();
    double getRad();
    double angle;
    double R;
private:
    double px;
    double py;
    double rad;
};


class asteroid: public QObject,
        public QGraphicsPixmapItem
{
    Q_OBJECT
public slots:
    void moveAsteroid();
    void removeDestroyedAst();

public:
    asteroid(QGraphicsItem* asteroid=0);
    ~asteroid();
    asteroidSim * getAsteroid();
    float getScale();
    void update(float dt);
    void position(double px_,double py_);
    QTimer *asteroid_timer;
    void calcAccAsteroids(float dt,planet &assignedPlanet);
    planet *assignedPlanet;
    void destroyAsteroid();
private:
    asteroidSim *asteroidsimulation;
    float scale=1;


};


#endif // ASTEROID_H
