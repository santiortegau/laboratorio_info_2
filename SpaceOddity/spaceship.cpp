#include "spaceship.h"
#include <QKeyEvent>
#include <qmath.h>
#define G 0.01


spaceshipSim::spaceshipSim()
{
    vx=0;
    vy=0;
    ax=0;
    ay=0;
    mass=200;

}

void spaceshipSim::setPos(double px_, double py_)
{
    px=px_;
    py=py_;
}
void spaceshipSim::setVel(double vx_,double vy_){
    vx=vx_;
    vy=vy_;
}
double spaceshipSim::getPx(){
    return px;
}
double spaceshipSim::getPy(){
    return py;
}
double spaceshipSim::getVx(){
    return vx;
}
double spaceshipSim::getVy(){
    return vy;
}
double spaceshipSim::getAx(){
    return ax;
}
double spaceshipSim::getAy(){
    return ay;
}
double spaceshipSim::getMass(){
    return mass;
}


void spaceship::moveSpaceship()
{
    double dx, dy;
    //Create a line between the spaceship and its starting station
    QLineF ln(QPointF(this->pos()),QPointF(DockedPort->pos()));
    //Stop the spaceship if nebulae has reduced its velocity to 0
    if (Vx<0 || Vy<0){
        spaceshipTimer->stop();
        movingSound->stop();
        emit gameover();
    }
    if(pos().x()>1400 || pos().y()>750 || pos().x()<-70 || pos().y()<-70){
        spaceshipTimer->stop();
        movingSound->stop();
        scene()->removeItem(this);
        emit gameover();
    }
    //Move the spacecraft according to its velocity and position
    dx=this->Vx*qSin(qDegreesToRadians(this->rotation()));
    dy=this->Vy*qCos(qDegreesToRadians(this->rotation()));
    setPos(this->x()+dx,this->y()-dy);
    //get a list of items colliding with the spacecraft
    QList<QGraphicsItem*> colliding_items=collidingItems();
    for (int i=0;i<colliding_items.size();i++){
        //Try to cast, to check if it is an asteroid
        asteroid *ast= dynamic_cast<asteroid*>(colliding_items[i]);
        //If effectively it is an asteroid destroy both the asteroid and the spacecraft emitting a gameover signal
        if(ast){
            ast->destroyAsteroid();
            scene()->removeItem(this);
            spaceshipTimer->stop();
            movingSound->stop();
            controlTimer->stop();
            QTimer *emitGO= new QTimer();
            emitGO->singleShot(1000,this,SIGNAL(gameover()));
            return;
        }
        //If it is colliding with nebulae reduce its velocity by a factor of 0.009 m/s
        if(typeid (*(colliding_items[i]))== typeid (nebula)){
            this->Vx=this->Vx-0.009;
            this->Vy=this->Vy-0.009;
            return;
        }
        //If it collides with powerups increase its velocity by a factor of 0.051 m/s
        powerups *pow= dynamic_cast<powerups*>(colliding_items[i]);
        if(pow){
            pow->powerTimer->stop();
            scene()->removeItem(pow);
            QTimer *boost= new QTimer();
            boost->singleShot(0,this,SLOT(boostVel()));
            return;
        }

        //If it is colliding with another spaceship remove them both and emit a gameover signal
        if(typeid (*(colliding_items[i]))== typeid (spaceship)){
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);
            spaceshipTimer->stop();
            movingSound->stop();
            controlTimer->stop();
            control->close();
            emit crashed();
            return;
        }
    }
    //If the spaceship has travelled more than 150px from its starting station start docking timer
    if(ln.length()>150.0 && !spaceshipDockingTimer->isActive()){
        spaceshipDockingTimer->start();
        //docking timer allows it to dock to a new station
        connect(spaceshipDockingTimer,SIGNAL(timeout()),this,SLOT(dock()));

    }
}

void spaceship::moveSpaceshipJoystick()
{
    char data;
    long long int l=0;
    //If asynchronous data is available to be captured
    if (control->bytesAvailable()>0){
        l=control->read(&data,1);
        switch (data) {
        case 'A':
            //Create new laser and orientate it according to the spaceship position
            laser= new ammo();
            laser->setOrientation(this->getOrientation());
            laser->setRotation(rotation());
            //Orientate the bullet when the spaceship is not moving
            if (this->orientation==1 && !spaceshipTimer->isActive()){
                laser->setPos(x(),y()+15);
            }
            else if (this->orientation==2 && !spaceshipTimer->isActive()){
                laser->setPos(x()+15,y());
            }
            else if (this->orientation==3 && !spaceshipTimer->isActive()){
                laser->setPos(x(),y()-15);
            }
            else if (this->orientation==4 && !spaceshipTimer->isActive()){
                laser->setPos(x()-15,y());
            }
            else{
                laser->setPos(x(),y());
            }
            this->scene()->addItem(laser);
            return;
        case '0':
            this->setRotation(rotation()-2.2);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
            return;
        case '1':
            this->setRotation(rotation()-1.5);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
            return;
        case'2':
            this->setRotation(rotation()-1);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
            return;
        case '3':
            this->setRotation(rotation()+1);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
            return;
        case'4':
            this->setRotation(rotation()+1.5);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
            return;
        case 'X':
            this->setRotation(rotation()+2.2);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
            return;
        case '5':
            //Stop orbiting a planet when launched command is called, also start moving the spaceship
            orbitTimer->stop();
            spaceshipTimer->start(10);
            movingSound->play();
            break;
        case 'N':
            return;
        default:
            return;
        }
    }

}

void spaceship::dock()
{

    QList<QGraphicsItem*> colliding_items=collidingItems();
    QList<spaceport*> spacePorts;
    //Create a line between this spaceship and the station it is docked
    QLineF ln(QPointF(this->pos()),QPointF(DockedPort->pos()));
    for (int i=0;i<colliding_items.size();i++){
        //Try to cast to see if it is colliding with a station
        spaceport *port=dynamic_cast<spaceport *>(colliding_items[i]);
        //If the casting was successful
        if(port){
            //Stop moving the spaceship
           this->spaceshipTimer->stop();
           movingSound->stop();
           //Assign that port to the spaceship's docked port
           DockedPort=port;
           //Add this port to a list to check it's possition
           spacePorts.append(port);
           //set a rotational target to rotate the spaceship according to the station orientation
           double rotTarget=spacePorts.last()->rotation();
           this->setRotation(rotTarget);
           //Center the spaceship on the station
           if(spacePorts.last()->getRandPos()==1){
               this->position(spacePorts.last()->getPx()+50.0,spacePorts.last()->getPy()+20.0);
           }
           if(spacePorts.last()->getRandPos()==2){
               this->position(spacePorts.last()->getPx()+25,spacePorts.last()->getPy()-50);
           }
           if(spacePorts.last()->getRandPos()==3){
               this->position(spacePorts.last()->getPx()-50,spacePorts.last()->getPy()-25);
           }
           if(spacePorts.last()->getRandPos()==4){
               this->position(spacePorts.last()->getPx()-25,spacePorts.last()->getPy()+50);
           }
           //stop the docking timer, since it has arrived to a new station
           spaceshipDockingTimer->stop();
           //If the spaceship has visited all the planets and it's docking to a port different from its principal
           //emit a winning signal
           if(planetsVisited.size()==planetsToVisit && DockedPort!=startingPort){
               spaceshipTimer->stop();
               movingSound->stop();
               controlTimer->stop();
               control->close();
               emit won();

           }
        }
    }
}

void spaceship::orbit()
{
    QList <QGraphicsItem*> itemsInScene= collidingItems();;
    for (int i=0;i<itemsInScene.size();i++){
        //Try to cast the colliding graphicsItem to a planet
        planet *planetToOrbit= dynamic_cast<planet*>(itemsInScene[i]);
        //If it's a planet add it to the visited planets list
        if (planetToOrbit){
            for(int i=0;i<planetsVisited.size()+1;i++){
                //If the planet has not been visited (does not appear on the list) add it
                if(planetsVisited.indexOf(planetToOrbit)==-1){
                    planetsVisited.append(planetToOrbit);
                    flag *visitedFlag= new flag();
                    //Positions the flag behind visitedPlanet
                    if(contr==1){
                        visitedFlag->setPos(planetToOrbit->x()+90,planetToOrbit->y()-25);
                        visitedFlag->setZValue(-10);
                        this->scene()->addItem(visitedFlag);
                    }
                    if(contr==2){
                        visitedFlag->setPos(planetToOrbit->x()-15,planetToOrbit->y()-15);
                        visitedFlag->setRotation(-40);
                        visitedFlag->setPixmap(QPixmap(":/flag2.png"));
                        visitedFlag->setZValue(-10);
                        this->scene()->addItem(visitedFlag);
                    }
                }
            }
            //This will be the new planet to orbit
            orbitingPlanet=planetToOrbit;
            //Create a line between the center of the planet and the spaceship's position
            QLineF Planetln(QPointF(planetToOrbit->getPlanet()->getPx()+planetToOrbit->getPlanet()->getRad(),planetToOrbit->getPlanet()->getPy()+planetToOrbit->getPlanet()->getRad()),
                      QPointF(this->pos()));
            //If this line is shorter than 80px (diameter of each planet) start to orbit that planet
            if (Planetln.length()<85){
                spaceshipTimer->stop();
                orbitTimer->start();
            }
        }

    }

}

void spaceship::orbitmove()
{
    //Slot called to start obiting around a planet
    movingSound->stop();
    //This function simulates rotational movement around  a planet
    this->calcAccSpaceship(0.01,this->orbitingPlanet);
    //If the aircraft collides with an asteroid while orbiting a planet remove it and emit a gameover signal
    QList<QGraphicsItem*> colliding_items=collidingItems();
    for (int i=0;i<colliding_items.size();i++){
        if(typeid(*(colliding_items[i]))==typeid (asteroid)){
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);
            spaceshipTimer->stop();     
            controlTimer->stop();
            control->close();
            emit gameover();
            delete colliding_items[i];
            return;
        }
        if(typeid (*(colliding_items[i]))== typeid (spaceship)){
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);
            spaceshipTimer->stop();
            movingSound->stop();
            controlTimer->stop();
            control->close();
            emit crashed();
            return;
        }
    }

}

void spaceship::GOver()
{
    //This slot is signaled from maingame class to stop timers of the spaceship and to close its serial port
    spaceshipTimer->stop();
    controlTimer->stop();
    control->close();
    spaceshipDockingTimer->stop();
    orbitTimer->stop();
    movingSound->stop();
}

void spaceship::boostVel()
{
    this->Vx=this->Vx+1;
    this->Vy=this->Vy+1;
    this->Vx=this->Vx+3;
    this->Vy=this->Vy+3;
    this->Vx=this->Vx+5;
    this->Vy=this->Vy+5;
    this->Vx=this->Vx-0.01;
    this->Vy=this->Vy-0.01;
    this->Vx=this->Vx-0.01;
    this->Vy=this->Vy-0.01;
    this->Vx=this->Vx-8.1;
    this->Vy=this->Vy-8.1;
}


spaceship::spaceship(int controller,QGraphicsItem *spaceship): QGraphicsPixmapItem (spaceship)
{
    this->setTransformOriginPoint(mapToItem(this,this->x(),this->y()));
    contr=controller;
    movingSound = new QMediaPlayer;
    movingSound->setMedia(QUrl("qrc:/movingspacecraft.mp3"));
    //If the spaceship is constructed to be used with a joystick
    if(controller==2){
        //Dissable keyboard for this object
        disableKeys=true;
        controlTimer= new QTimer(this);
        connect(controlTimer,SIGNAL(timeout()),this,SLOT(moveSpaceshipJoystick()));
        setPixmap(QPixmap(":/spacex2.png"));
        control = new QSerialPort();
        control->setPortName("COM5");
        if(control->open(QIODevice::ReadWrite)){
            if(!control->setBaudRate(QSerialPort::Baud9600))
                qDebug()<<control->errorString();

            if(!control->setDataBits(QSerialPort::Data8))
                qDebug()<<control->errorString();

            if(!control->setParity(QSerialPort::NoParity))
                qDebug()<<control->errorString();

            if(!control->setStopBits(QSerialPort::OneStop))
                qDebug()<<control->errorString();

            if(!control->setFlowControl(QSerialPort::NoFlowControl))
                qDebug()<<control->errorString();
                controlTimer->start();
            }
    }
    if (controller==1){
        setPixmap(QPixmap(":/spacex1.png"));
        controlTimer= new QTimer(this);
        control =new QSerialPort();
    }
    spaceshipSimulation= new spaceshipSim();
    spaceshipTimer= new QTimer(this);
    spaceshipTimer->stop();
    connect(spaceshipTimer,SIGNAL(timeout()),this,SLOT(moveSpaceship()));
    spaceshipDockingTimer= new QTimer(this);
    spaceshipDockingTimer->stop();
    //This flag allows the object to be focused by clicking on it
    this->setFlag(QGraphicsItem::ItemIsFocusable);
    orbitTimer= new QTimer(this);
    orbitTimer->stop();
    connect(spaceshipTimer,SIGNAL(timeout()),this,SLOT(orbit()));
    connect(orbitTimer,SIGNAL(timeout()),this,SLOT(orbitmove()));

}

spaceship::~spaceship()
{
    delete spaceshipSimulation;
    delete spaceshipTimer;
    delete spaceshipDockingTimer;
    delete orbitTimer;
    delete laser;
    delete movingSound;
}

spaceshipSim *spaceship::getSpaceship()
{
    return spaceshipSimulation;
}

void spaceship::position(double px_, double py_)
{
    this->getSpaceship()->setPos(px_,py_);
    setPos(px_,py_);
}

void spaceship::calcAccSpaceship(double dt, planet * orbitPlanet)
{
    //Create a line between the center of the planet and the spaceship's front
    QLineF ln(QPointF(pos()),QPointF(orbitPlanet->getPlanet()->getPx()+40,orbitPlanet->getPlanet()->getPy()+40));
    //update its delta angle and position the spaceship around a circular trajectory
    //Gravitational constant G
    orbitalAngle+=(qSqrt(G*3/ln.length()))*dt;
    position(orbitPlanet->getPlanet()->getPx()+orbitPlanet->getPlanet()->getRad()+(120*qCos(orbitalAngle)),orbitPlanet->getPlanet()->getPy()+orbitPlanet->getPlanet()->getRad()+(120*qSin(orbitalAngle)));

}

void spaceship::setOrientation(int ori)
{
    orientation=ori;
    if(orientation==1){
        setRotation(90);
    }
    if(orientation==3){
        setRotation(270);
    }
    if(orientation==4){
        setRotation(180);
    }
}

int spaceship::getOrientation()
{
    return orientation;
}

void spaceship::keyPressEvent(QKeyEvent *event)
{
    //If keyboard is not disabled catch a keyboard event
    if(!disableKeys){
        if (event->key()==Qt::Key_Space){
            orbitTimer->stop();
            spaceshipTimer->start(10);
            movingSound->play();
        }
        if (event->key()==Qt::Key_Left){
            this->setRotation(rotation()-5);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
        }
        if (event->key()==Qt::Key_Right){
            this->setRotation(rotation()+5);
            this->setPos(pos());
            this->getSpaceship()->setPos(x(),y());
        }
        if (event->key()==Qt::Key_B){
            laser= new ammo();
            laser->setOrientation(this->getOrientation());
            laser->setRotation(rotation());
            if (this->orientation==1 && !spaceshipTimer->isActive()){
                laser->setPos(x(),y()+15);
            }
            else if (this->orientation==2 && !spaceshipTimer->isActive()){
                laser->setPos(x()+15,y());
            }
            else if (this->orientation==3 && !spaceshipTimer->isActive()){
                laser->setPos(x(),y()-15);
            }
            else if (this->orientation==4 && !spaceshipTimer->isActive()){
                laser->setPos(x()-15,y());
            }
            else{
                laser->setPos(x(),y());
            }
            this->scene()->addItem(laser);
        }
    }
}

string spaceship::getName()
{
    return name;
}

void spaceship::setName(string name_)
{
    name=name_;
}

