#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QWidget>
#include <qgraphicsscene.h>
#include <fstream>
#include <iostream>
using namespace std;
#include <maingame.h>
#include <tutorial.h>

namespace Ui {
class registration;
}

class registration : public QWidget
{
    Q_OBJECT

public:
    explicit registration(QWidget *parent = nullptr);
    ~registration();

private slots:
    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_lineEdit_textEdited(const QString &arg1);

    void on_lineEdit_2_textEdited(const QString &arg1);

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_8_clicked();

    void startGame();

private:
    Ui::registration *ui;
    QGraphicsScene *scene;
    QString player1;
    QString player2;
    int gameMode;
    int controller;
    int level;
    ofstream fout;
    void closeEvent(QCloseEvent *event);

};

#endif // REGISTRATION_H
