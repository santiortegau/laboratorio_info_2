#include <iostream>

using namespace std;
/* This program gets an integer and returns the n'th prime number.
 * REQUIREMENTS:
 *  -Integer number greater than zero.
 * GUARANTEES:
 *  -The return of the n'th prime number.
 */

int main()
{
    int n,cont,nesimo=0,nprime=0;
    cout<<"Ingrese un numero: "<<endl;
    cin>>n;
    int i =2;
    while (i!=nesimo){              //i starts from 2 given that it's the first prime number
        cont=0;
        for (int j=1;j<=i;j++){     //This loop uses an if statement to find the divisors of each (i) number and increments cont if true.
            if (i%j==0) cont++;
        }
        if (cont<3){                //After finding its divisors if they are exactly 2, i is the n'th prime number.
            nprime=i;
            nesimo++;               //Increment the position of the n'th prime number until it's equal to the given number.

            if (nesimo==n) cout<<"El numero primo No. "<< n<<" es: "<<nprime<<endl;
        }
        i++;                        //Increment i to evaluate a new number


    }

}
