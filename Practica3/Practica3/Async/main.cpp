#include <iostream>
#include<fstream>
#include <string.h>
#include <string>
#include <QDebug>
#include <QTextStream>
#include <QFile>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <vector>
#include "funcionesej2.h"

using namespace std;

QSerialPort serial;

bool serialInit(void);
void writeData(vector<char> V);

int main(){

    bool flag = true;
    char data;
    int l = 0;


    int state=0;
    int cont = 0;
    vector<char> informacion;

    if(serialInit()){
        cout<<"Estado 0"<<endl;

        while(flag){
            if(serial.waitForReadyRead(-1)){
                //Data was returned
                l = serial.read(&data,1);
                cout<<data<<endl;
                switch (state) {
                    case 0:
                        if(data=='1'){
                            cont++;
                        }else{
                            cont = 0;
                        }

                        if(cont==16){

                            cont = 0;
                            state++;
                        }
                        break;

                    case 1:
                        informacion.push_back(data);

                        if(data=='0'){
                            cont++;
                        }else{
                            cont=0;
                        }

                        if(cont == 16){

                            state++;
                        }
                        break;

                    case 2:
                        if(data=='1'){
                            informacion.push_back(data);
                        }else{
                            informacion.erase(informacion.end()-15,informacion.end());
                            flag = false;
                            state = 0;
                        }
                        break;
                    default:
                        break;
                }

            }else{
                //No data
                qDebug()<<"Time out";
            }
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        serial.close();
        writeData(informacion);

        for (int i=0;i<informacion.size();i++){
            cout<<informacion[i];
        }
        cout<<endl;
        string str="";
        for (int i=0;i<informacion.size();i++){
            str+=informacion[i];
        }
        int seed;
        cout<<"Ingrese la semilla a utilizar: "<<endl;
        cin>>seed;
        cout<<convfromBin(decodeM2(seed,str))<<endl;
    }
    return 0;
}


bool serialInit(void){
    serial.setPortName("COM5"); //Poner el nombre del puerto, probablemente no sea COM3

    if(serial.open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!serial.setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<serial.errorString();

        if(!serial.setDataBits(QSerialPort::Data8))
            qDebug()<<serial.errorString();

        if(!serial.setParity(QSerialPort::NoParity))
            qDebug()<<serial.errorString();

        if(!serial.setStopBits(QSerialPort::OneStop))
            qDebug()<<serial.errorString();

        if(!serial.setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<serial.errorString();
    }else{
        qDebug()<<"Serial COM3 not opened. Error: "<<serial.errorString();
        return false;
    }
    return true;
}

void writeData(vector<char> V){
    unsigned int i;

    ofstream archivo;
    archivo.open("salida.txt");

    for(i=0;i<V.size();i++){
        archivo<<V[i];
    }

    archivo.close();
}



