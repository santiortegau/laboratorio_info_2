#include <iostream>
#include <fstream>

using namespace std;
string *split(string str,string *blocks);
void blocksSort(string *blocks,string *copy);

int main()
{
    ofstream output;
    ifstream input;

    cout<<"Ingrese el nombre del archivo fuente: "<<endl;
    string inputFilename;
    cin>>inputFilename;
    cout<<"Ingrese el nombre del archivo de salida: "<<endl;
    string outputFilename;
    cin>>outputFilename;

    try {
        input.open(inputFilename);
        if(!input.is_open()) throw '1';
        //No olvidar cerrar el archivo al final del main
        cout<<"Caracteres a codificar: "<<endl;
        string str,uncodedStr;
        while (input.good()){
            getline(input,str);
            uncodedStr+=str;
            cout<<uncodedStr<<endl;
        }
        string blocks[200];
        split(uncodedStr,blocks);
        string blocksCopy[200];
//        for (int i=0;i<16;i++){
//            blocksCopy[i]=blocks[i];
//        }
        output.open(outputFilename);
        if(!output.is_open()) throw '2';
        blocksSort(blocks,blocksCopy);
        cout<<endl;
        cout<<"Caracteres organizados de menor a mayor (por cant. vocales): "<<endl;
        string sorted="";
        for (int  i=0;i<200;i++){
            if (blocksCopy[i]!=""){
                cout<<blocksCopy[i]<<" ";
                sorted+=blocksCopy[i]+" ";
            }

        }
        output<<sorted<<endl;
        output.close();
        input.close();

    } catch (char e) {
        cout<<"Error numero: ";
        if (e=='1') cout<<"1: Error abriendo el archivo de entrada"<<endl;
    }
    catch(...) {
        cout<<"Error no identificado"<<endl;
    }
}
string *split(string str,string *blocks){
    string aux="";
    unsigned int len=str.length();
    int k=0;
    for (unsigned int i=0;i<len;i++){
        if (str[i]!=' '){
            aux+=str[i];
        }
        else{
            blocks[k]=aux;
            k++;
            aux="";
        }
    }
    blocks[k]=aux;

    return blocks;

}
void blocksSort(string *blocks,string *copy){
    int contVocales=0;
    for (int i =0;i<16;i++){
        for (int j=0;j<blocks[i].length();j++){
            if (blocks[i][j]=='a'||blocks[i][j]=='e'||blocks[i][j]=='i'||blocks[i][j]=='o'||blocks[i][j]=='u'||blocks[i][j]=='A'||blocks[i][j]=='E'||blocks[i][j]=='I'||blocks[i][j]=='O'||blocks[i][j]=='U'){
                contVocales++;
            }
        }
        if (copy[contVocales]!="")contVocales++;
        copy[contVocales]=blocks[i];
        contVocales=0;
    }
}

