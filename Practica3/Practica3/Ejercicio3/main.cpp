#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream file1;
    ifstream file2;

    try {
        file1.open("C:/Users/santi/Dropbox/UdeA/5th Semester/Informatica II/laboratorio_info_2/Practica3/Practica3/build-Ejercicio1-Desktop_Qt_5_11_1_MinGW_32bit-Debug/entrada.txt");
        file2.open("C:/Users/santi/Dropbox/UdeA/5th Semester/Informatica II/laboratorio_info_2/Practica3/Practica3/build-Ejercicio2-Desktop_Qt_5_11_1_MinGW_32bit-Debug/salida.txt");
        if (!file1.is_open()) throw '1';
        if (!file2.is_open()) throw '2';
        //file1.getline(comp1,100);
        //file2.getline(comp2,100);
        string comp1Str,str1;
        string comp2Str,str2;
        while (file1.good()){
            getline(file1,comp1Str);
            str1+=comp1Str;
        }
        while (file2.good()){
            getline(file2,comp2Str);
            str2+=comp2Str;
        }

        unsigned int cont=0;
        string diff;
        if (str1.length()==str2.length()){
            for (unsigned int i=0;i<str1.length();i++){
                if (str1[i]==str2[i]) cont++;
                else diff+=str1[i];
            }
            if (cont==str1.length()) cout<<"La palabra es igual."<<endl;
            else{
                cout<<"La palabra no es igual, difiere en las letras: ";
                for (unsigned int i=0;i<diff.length();i++) cout<<diff[i]<<", ";
                cout<<endl;
            }

        } else {
            cout<<"La palabra no es igual, difiere por longitud en las letras: ";
            if (str1.length()>str2.length()){
                for (unsigned int i=str2.length();i<str1.length();i++){
                    cout<<str1[i]<<", ";
                }
            }
            if (str1.length()<str2.length()){
                for (unsigned int i=str1.length();i<str2.length();i++){
                    cout<<str2[i]<<", ";
                }
            }
            cout<<endl;
         }

    } catch (char e) {
        cout<<"Error numero: ";
        if (e=='1') cout<<"1: Error abriendo el archivo 1."<<endl;
        if (e=='2') cout<<"2: Error abriendo el archivo 2."<<endl;
    }
    catch (...){
        cout<<"Error no identificado."<<endl;
    }

}
