#include <iostream>
#include <fstream>


using namespace std;

int main()
{
    char cadena[15];

    ofstream fout;
    ifstream fin;

    try{


        cout<<"Digite una palabra: ";
        cin>>cadena;

        fout.open("texto.txt");
        if(!fout.is_open()){
            throw '1';
        }
        fout<<cadena;
        fout.close();

        fin.open("texto.txt");
        if (!fin.is_open()){
            throw '2';
        }

        int i=0;
        char temp;
        while(fin.good()){
            temp=fin.get(); //se guarda en la posicion 0
            if (fin.good()){
                cadena[i]=temp;
            }
            i++;
        }
        cout<<"Texto Leido: "<<endl;
        cout<<cadena<<endl;
    }
    catch   (char e){
        cout<<"Error numero: ";
        if(e=='1'){
            cout<<"1: Error de salida."<<endl;
        }
        else if (e=='2'){
            cout<<"2: Error de entrada"<<endl;
        }
    }
    catch(...){
        cout<<"Algo esta fallando"<<endl;
    }

    return 0;
}
