#include <iostream>
#include <fstream>

using namespace std;
string codeM1(unsigned int,string);
string convtoBin(string);
string intTobinStr(int);
string *split(unsigned int n, string str,string *);
string codeM2(unsigned int n, string str);
unsigned int lastBlockOneCount(string str);

int main()
{
    ofstream output;
    ifstream input;

    cout<<"Ingrese el nombre del archivo fuente: "<<endl;
    string inputFilename;
    cin>>inputFilename;
    cout<<"Ingrese el nombre del archivo de salida: "<<endl;
    string outputFilename;
    cin>>outputFilename;
    cout<<"Ingrese la semilla de codificacion: "<<endl;
    unsigned int seed;
    cin>>seed;
    cout<<"Ingrese el metodo de codificacion (1 o 2): "<<endl;
    int mode;
    cin>>mode;

    try {
        input.open(inputFilename);
        if(!input.is_open()) throw '1';
        //No olvidar cerrar el archivo al final del main
        cout<<"Caracteres a codificar: "<<endl;
        string str,uncodedStr;
        while (input.good()){
            getline(input,str);
            uncodedStr+=str;
            cout<<uncodedStr<<endl;
            if (input.good()) uncodedStr+="\n";
        }

        output.open(outputFilename);
        if(!output.is_open()) throw '2';
        uncodedStr=convtoBin(uncodedStr);

        if (mode==1){
            cout<<"Codificado con el metodo 1: "<<endl;
            cout<<codeM1(seed,uncodedStr)<<endl;
            output<<codeM1(seed,uncodedStr)<<endl;
        }
        if (mode==2){
            cout<<"Codificado con el metodo 2: "<<endl;
            cout<<codeM2(seed,uncodedStr)<<endl;
            output<<codeM2(seed,uncodedStr)<<endl;
        }
        output.close();
        input.close();

    } catch (char e) {
        cout<<"Error numero: ";
        if (e=='1') cout<<"1: Error abriendo el archivo de entrada"<<endl;
    }
    catch(...) {
        cout<<"Error no identificado"<<endl;
    }
}
string codeM1(unsigned int n,string str){
    string codedStr="";
    string blocks[1000];
    split(n,str,blocks);
    string blocksOriginal[1000];
    for (int i=0;i<1000;i++){
        blocksOriginal[i]=blocks[i];
    }
    for(unsigned int i=0;i<n;i++){  //Cambia el primer bloque
        if (blocks[0][i]=='0') blocks[0][i]='1';
        else blocks[0][i]='0';
    }

    unsigned int blocksCount=0;
    if(str.length()%n!=0) blocksCount=(str.length()/n)+1;
    else blocksCount=str.length()/n;
    //Agregar codigo para llenar los bloques si no son de 8 caracteres (ver funcion intobinstr)
    if (str.length()%n!=0) blocksCount--;
    for (unsigned int i=0;i<blocksCount-1;i++){ //Aplica las 3 reglas a los bloques subsecuentes
        unsigned int ones=lastBlockOneCount(blocksOriginal[i]);
        unsigned int ceros=n-ones;
        if (ones==ceros){
            for (unsigned int j=0;j<n;j++){
                if (blocks[i+1][j]=='1') blocks[i+1][j]='0';
                else blocks[i+1][j]='1';
            }
        }
        if (ones<ceros){
            for (unsigned int j=1;j<n;j+=2){
                if (blocks[i+1][j]=='0') blocks[i+1][j]='1';
                else blocks[i+1][j]='0';
            }
        }
        if (ones>ceros){
            for (unsigned int j=2;j<n;j+=3){
                if (blocks[i+1][j]=='0') blocks[i+1][j]='1';
                else blocks[i+1][j]='0';
            }
        }

    }
    for(unsigned int i=0;i<=blocksCount;i++){ //Genera el string codificado
        codedStr+=blocks[i];
    }
    return codedStr;
}

string codeM2(unsigned int n, string str){
    string codedStr="";
    string blocks[1000];
    split(n,str,blocks);
    string blocksOriginal[1000];
    for (int i=0;i<1000;i++){
        blocksOriginal[i]=blocks[i];
    }
    unsigned int blocksCount=0;
    if(str.length()%n!=0) blocksCount=(str.length()/n)+1;
    else blocksCount=str.length()/n;

    for (unsigned int i=0;i<=blocksCount-1;i++){
        for(int j=blocks[i].length()-1;j>=0;j--){
            if (j==0){
                blocks[i][j]=blocksOriginal[i][blocks[i].length()-1];
                break;
            }
            blocks[i][j]=blocks[i][j-1];
        }
    }
    for(unsigned int i=0;i<blocksCount;i++){ //Genera el string codificado
        codedStr+=blocks[i];
    }
    return codedStr;
}

string convtoBin(string letras){
    string binaryCode="";
    int aux;
    for (unsigned int i=0;i<letras.length();i++){
        aux = int (letras[i]);
        binaryCode+="0"+intTobinStr(aux);
    }
    return binaryCode;
}
string intTobinStr(int n){
    int binaryNum[1000];
    string bin="",intBin;

    int k=0;
    do{
        binaryNum[k]=n%2;
        if (binaryNum[k]==1){
            bin+="1";
        }
        else bin+="0";
        n/=2;
        k++;
    } while (n>0);
    for (int j=k-1;j>=0;j--){ //sobreescribe el binario al reves
        intBin+=bin[j];
    }
    if (intBin.length()==7) return intBin; //Si la longitud del binary no es de 7 inserte ceros al principio para llenarlo
    else{
        while (intBin.length()<7){
            intBin.insert(0,"0");
        }
    }

}
string *split(unsigned int n, string str,string *blocks){
    string aux="";
    unsigned int len=str.length();
    if (len%n==0){
        for (unsigned int i=0;i<len/n;i++){
                for(unsigned int j=0;j<n;j++){
                    aux+=str[j];
                }
                blocks[i]=aux;
                if (str.length()==0) break;
                str.erase(0,n);
                aux="";
        }
    }
    else{
        for (unsigned int i=0;i<(len/n)+1;i++){
            if (str.length()>=n){
                for(unsigned int j=0;j<n;j++){
                    aux+=str[j];
                }
                blocks[i]=aux;
                if (str.length()==0) break;
                str.erase(0,n);
                aux="";
            }else blocks[i]=str;
    }
    return blocks;
    }
}
unsigned int lastBlockOneCount(string str){
    unsigned int cont=0;
    for (unsigned int i=0;i<str.length();i++){
        if (str[i]=='1') cont++;
    }
    return  cont;
}





















