#include "ejercicio2.h"
#include <iostream>
using namespace std;


string decodeM1(unsigned int n,string str){
    string decodedStr="";
    string blocks[1000];
    split(n,str,blocks);
    string blocksOriginal[1000];
    for (int i=0;i<1000;i++){
        blocksOriginal[i]=blocks[i];
    }
    for(unsigned int i=0;i<n;i++){  //Cambia el primer bloque
        if (blocks[0][i]=='0') blocks[0][i]='1';
        else blocks[0][i]='0';
    }

    unsigned int blocksCount=0;
    if(str.length()%n!=0) blocksCount=(str.length()/n)+1;
    else blocksCount=str.length()/n;
    if (str.length()%n!=0) blocksCount--;
    for (unsigned int i=0;i<blocksCount-1;i++){ //Aplica las 3 reglas a los bloques subsecuentes
        unsigned int ones=lastBlockOneCount(blocks[i]);
        unsigned int ceros=n-ones;
        if (ones==ceros){
            for (unsigned int j=0;j<n;j++){
                if (blocks[i+1][j]=='1') blocks[i+1][j]='0';
                else blocks[i+1][j]='1';
            }
        }
        if (ones<ceros){
            for (unsigned int j=1;j<n;j+=2){
                if (blocks[i+1][j]=='0') blocks[i+1][j]='1';
                else blocks[i+1][j]='0';
            }
        }
        if (ones>ceros){
            for (unsigned int j=2;j<n;j+=3){
                if (blocks[i+1][j]=='0') blocks[i+1][j]='1';
                else blocks[i+1][j]='0';
            }
        }

    }
    for(unsigned int i=0;i<=blocksCount;i++){ //Genera el string codificado
      //  if ()
        decodedStr+=blocks[i];
    }
    return decodedStr;
}
string decodeM2(unsigned int n, string str){
    string decodedStr="";
    string blocks[1000];
    split(n,str,blocks);
    string blocksOriginal[1000];
    for (int i=0;i<1000;i++){
        blocksOriginal[i]=blocks[i];
    }
    unsigned int blocksCount=0;
    if(str.length()%n!=0) blocksCount=(str.length()/n)+1;
    else blocksCount=str.length()/n;

    for (unsigned int i=0;i<=blocksCount-1;i++){
        for(int j=0;j<=blocks[i].length()-1;j++){
            if (j==blocks[i].length()-1){
                blocks[i][j]=blocksOriginal[i][0];
                break;
            }
            blocks[i][j]=blocks[i][j+1];
        }
    }
    for(unsigned int i=0;i<blocksCount;i++){ //Genera el string codificado
        decodedStr+=blocks[i];
    }
    return decodedStr;
}
string *split(unsigned int n, string str,string *blocks){
    string aux="";
    unsigned int len=str.length();
    if (len%n==0){
        for (unsigned int i=0;i<len/n;i++){
                for(unsigned int j=0;j<n;j++){
                    aux+=str[j];
                }
                blocks[i]=aux;
                if (str.length()==0) break;
                str.erase(0,n);
                aux="";
        }
    }
    else{
        for (unsigned int i=0;i<(len/n)+1;i++){
            if (str.length()>=n){
                for(unsigned int j=0;j<n;j++){
                    aux+=str[j];
                }
                blocks[i]=aux;
                if (str.length()==0) break;
                str.erase(0,n);
                aux="";
            }else blocks[i]=str;
    }
    return blocks;
    }
}
unsigned int lastBlockOneCount(string str){
    unsigned int cont=0;
    for (unsigned int i=0;i<str.length();i++){
        if (str[i]=='1') cont++;
    }
    return  cont;
}
string convfromBin(string binary){
    string words="";
    string binaryBlocks[100];
    split(8,binary,binaryBlocks);
    int aux=0;
    for (unsigned int i=0;i<99;i++){
        if (binaryBlocks[i]!=""){
            aux=stoi(binaryBlocks[i],nullptr,2);
            words+=char (aux);
        }
        else break;
    }
    return words;
}
