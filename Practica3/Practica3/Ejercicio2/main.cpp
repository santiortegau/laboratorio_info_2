#include <iostream>
#include <fstream>
#include "ejercicio2.h"

using namespace std;


int main()
{
    ofstream output;
    ifstream input;

    //cout<<"Ingrese el nombre del archivo fuente: "<<endl;
    //string inputFilename;
    //cin>>inputFilename;
    cout<<"Ingrese el nombre del archivo de salida: "<<endl;
    string outputFilename;
    cin>>outputFilename;
    cout<<"Ingrese la semilla de codificacion: "<<endl;
    unsigned int seed;
    cin>>seed;
    cout<<"Ingrese el metodo de codificacion (1 o 2): "<<endl;
    int mode;
    cin>>mode;

    try {
        char coded[1000]="";
        input.open("C:/Users/santi/Dropbox/UdeA/5th Semester/Informatica II/laboratorio_info_2/Practica3/Practica3/build-Ejercicio1-Desktop_Qt_5_11_1_MinGW_32bit-Debug/salida.txt");
        if(!input.is_open()) throw '1';
        //No olvidar cerrar el archivo al final del main
        //cin.ignore();
        input.getline(coded,1000);
        cout<<"Codigo binario a descodificar: "<<endl;
        cout<<coded<<endl;

        string codedStr="";
        for (int i=0;i<999;i++){
            while(coded[i]!='\0'){
                codedStr+=coded[i];
                break;
            }
        }
        output.open(outputFilename);
        if(!output.is_open()) throw '2';

        if (mode==1){
            cout<<"Decodificado con el metodo 1: "<<endl;
            cout<<convfromBin(decodeM1(seed,codedStr))<<endl;
            output<<convfromBin(decodeM1(seed,codedStr))<<endl;
        }
        if (mode==2){
            cout<<"Decodificado con el metodo 2: "<<endl;
            cout<<convfromBin(decodeM2(seed,codedStr))<<endl;
            output<<convfromBin(decodeM2(seed,codedStr))<<endl;
        }
        output.close();
        input.close();

    } catch (char e) {
        cout<<"Error numero: ";
        if (e=='1') cout<<"1: Error abriendo el archivo de entrada"<<endl;
    }
    catch(...) {
        cout<<"Error no identificado"<<endl;
    }

}

