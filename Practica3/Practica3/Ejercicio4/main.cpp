#include <QDebug>
#include <windows.h>
#include <QTextStream>
#include <QFile>
#include <QtSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <iostream>
using namespace std;
#include "funcionesej2.h"

int main(){

    QSerialPort serial;
    serial.setPortName("COM5"); //Poner el nombre del puerto, probablemente no sea COM3

    if(serial.open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!serial.setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<serial.errorString();
//        if(!serial.setDataBits(QSerialPort::Data8))
//                   qDebug()<<serial.errorString();

//        if(!serial.setParity(QSerialPort::NoParity))
//                   qDebug()<<serial.errorString();

//        if(!serial.setStopBits(QSerialPort::OneStop))
//                   qDebug()<<serial.errorString();

//        if(!serial.setFlowControl(QSerialPort::NoFlowControl))
//                   qDebug()<<serial.errorString();

        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //Asincrona
        char flat = 1;
        char data;
        long long int l = 0;
        string str;
        while(flat){
            if(serial.waitForReadyRead(1000)){
                //Data was returned
                l = serial.read(&data,1);
                int contUnos=0;
                int contCeros=0;
                while (contUnos!=16){
                    serial.read(&data,1);
                    if (data=='1') contUnos++;
                    else contUnos=0;
                }
                serial.read(&data,1);
                while (contCeros!=16){
                    serial.read(&data,1);
                    if (data=='0') contCeros++;
                    else contCeros=0;
                    str+=data;
                    qDebug()<<"Response: "<<data;
                }

                str.erase(str.length()-16);

            }else{
               //No data
                qDebug()<<"Time out";
            }
        }
        cout<<str<<endl;
        cout<<convfromBin(decodeM1(4,str))<<endl;
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        serial.close();
    }else{
        qDebug()<<"Serial COM3 not opened. Error: "<<serial.errorString();
    }
    return 0;
}
