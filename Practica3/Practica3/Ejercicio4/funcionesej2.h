#ifndef EJERCICIO2_H
#define EJERCICIO2_H
#include<iostream>
using namespace std;


string decodeM1(unsigned int n,string str);
string decodeM2(unsigned int n, string str);
string *split(unsigned int n, string str,string *result);
unsigned int lastBlockOneCount(string str);
string convfromBin(string);

#endif // EJERCICIO2_H
