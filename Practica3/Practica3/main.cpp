#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ofstream fout;
    ifstream fin;

    char palabra[15]="Hola";
    char palabra2[15];

    try{


        fout.open("ejemplo.txt");
        if (!fout.is_open()){
            throw '1';
        }

        while(palabra[0]!='\0'){
            cout<<"Digite su nombre: ";
            cin.getline(palabra,15); //Guarda la linea completa
            fout<<palabra<<endl;
        }

        fout.close();
        fin.open("ejemplo.txt");
        if (!fin.is_open()){
            throw '2';
        }
        cout<<"Lectura: "<<endl;
        while (fin.good()){ //se torna falso cuando llega al final del archivo
            fin.getline(palabra2,15);
            cout<<palabra2<<endl;
        }
    }
    catch (char e){
        cout<<"Error numero: ";
        if (e=='1'){
            cout<<"1: Error abriendo el archivo de salida"<<endl;
        }
        else if(e=='2'){
              cout<<"2: Error abiendo el archivo de entreada"<<endl;
        }
    }
    catch(...){
        cout<<"Error no identificado"<<endl;
    }

}
