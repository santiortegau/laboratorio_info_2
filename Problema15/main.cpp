#include <iostream>

using namespace std;
/* This program gets an odd number less than 10, prints a spiral matrix (nxn) and calculates the
 * sum of its diagonal and anti-diagonal .
 * REQUIREMENTS:
 *  -Odd Number less than 10.
 * GUARANTEES:
 *  -Prints a spiral outwards matrix and calculates the sum of its diagonal and anti-diagonal.
 */

int main() {
        int n,sum=0, num=1;                          //Num is the variable which writes each number in the matrix
        cout<<"Ingrese un numero Impar: "<<endl;
        cin>>n;
        int x = 0;                                   // current x position in the matrix
        int y = 0;                                   // current y position in the matrix
        int direct = 0;                              // current direction of printing, 0=RIGHT, 1=DOWN, 2=LEFT, 3=UP
        int s = 1;                                   // define the printing chain size
        int matrix[n][n];                            // Define the matrix with the input value
        x = n/2;                                     // starting point in the middle of the matrix
        y = n/2;

        for (int k=1; k<=n-1; k++)                   //Loop that checks the values to print until it reaches the last -1 number
        {
            for (int j=0; j<(k<(n-1)?2:3); j++)      //Ternary operator if (k<(n-1)) return 2 and compare it as (j<2), else compare it as (j<3)
            {
                for (int i=0; i<s; i++)              //Whenever the numbers being printed are less than the max numbers per chain
                {
                    matrix[x][y]=num;                //Assign the num value to the matrix in x, y, position
                    num++;

                    switch (direct)                  //According to the direction being printed, x or y are decreased or increased to move through the matrix.
                    {
                        case 0: y = y + 1; break;    //In case it's going right increase the y coordenate
                        case 1: x = x + 1; break;    //In case it's going down increase x and move 1 row below
                        case 2: y = y - 1; break;    //To move left decrease the column index "y"
                        case 3: x = x - 1; break;    //To move up in clockwise direction, decrease row index "x"
                    }
                }
                direct = (direct+1)%4;              //Increase direct whenever the chain reaches its max. Module is to achieve only 4 directions
            }
            s = s + 1;
        }
        matrix[x][y]=n*n;                           //Add the last number to the last position in the spiral
            for (int i =0;i<=n-1;i++){              //For each row and each column print the matrix
                for (int j=0;j<=n-1;j++){
                    if ((matrix[i][j])<10){         //If the number to print is less than 10, add spaces in between to make it appear as a squared matrix
                        cout<<" "<<matrix[i][j]<<" ";
                    }
                    else{
                        cout<<matrix[i][j]<<" ";
                    }

                }
                cout<<endl;
            }
                for (int i=0,j=0;i==j&&i<=n-1;i++,j++) {        //For every same row and column index, add the sum of the values of the diagonal
                        sum+=matrix[i][j];

                }
                for (int i=0,j=n-1;i<=n-1;i++,j--){             //starting in the upper right position increase the row index and decrease the column index to sum the antidiagonal values
                    sum+=matrix[i][j];
                }
                sum-=matrix[n/2][n/2];                          //Substract the middle value which has been added twice

                   cout<<"\nLa suma de la diagonal es: "<<sum<<endl;

    }
