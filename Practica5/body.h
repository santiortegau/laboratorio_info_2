#ifndef BODY_H
#define BODY_H
#define g -9.8
#define G 1
#include <cmath>
#include <iostream>
#include <vector>
using namespace std;

class Body
{
public:
    Body(string name_,float mass_,float px_=0,float py_=0,float vx_=0,float vy_=0,float ax_=0,float ay_=0);
    float getPx();
    float getPy();
    float getMass();
    void newPos(float time);
    float ax;
    float ay;
    string name;
private:
    float px;
    float py;
    float vx;
    float vy;

    float mass;
};

#endif // BODY_H
