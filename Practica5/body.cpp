#include "body.h"

Body::Body(string name_,float mass_,float px_,float py_,float vx_,float vy_,float ax_,float ay_)
{
    mass=mass_;
    px=px_;
    py=py_;
    vx=vx_;
    vy=vy_;
    ax=ax_;
    ay=ay_;
    name=name_;
}
float Body::getPx() {
    return px;
}
float Body::getPy(){
    return py;
}
float Body::getMass(){
    return mass;
}
void Body::newPos(float time){
    vx+=ax*time;
    vy+=ay*time;
    px+=vx*time+(ax*time*time)/2;
    py+=vy*time+(ay*time*time)/2;

}
