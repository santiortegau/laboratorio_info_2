#include <iostream>
#include "body.h"
#include <fstream>

using namespace std;
void calcAcc(Body &body,vector<Body> bodies);

int main()
{
    vector<Body>bodies;
    while (true){
        cout<<"--GRAVITATIONAL SYSTEM SIMULATION--"<<endl<<endl;
        cout<<"Choose one of the following options: "<<endl<<"1. Add objects."<<endl<<"2. Simulate System."<<endl<<"3. To exit."<<endl;
        cout<<"Input (ONLY USE INTEGER NUMBERS): ";
        int option;
        cin>>option;
        cout<<endl<<endl;
        if(option==1){
            cout<<"Enter number of objects to be added: ";
            int n;
            cin>>n;
            float mass,x0,y0,vx0,vy0;
            cout<<endl<<endl;
            for (int i=1;i<=n;i++){
                cout<<"For object number: "<<i<<endl;
                cout<<"Mass: ";
                cin>>mass;
                cout<<endl;
                cout<<"Init X: ";
                cin>>x0;
                cout<<endl;
                cout<<"Init Y: ";
                cin>>y0;
                cout<<endl;
                cout<<"Init Vx: ";
                cin>>vx0;
                cout<<endl;
                cout<<"Init Vy: ";
                cin>>vy0;
                cout<<endl;
                Body tempBody(to_string(i),mass,x0,y0,vx0,vy0);
                bodies.push_back(tempBody);
            }
        }
        if(option==2){
            cout<<"Enter dt for the Simulation: ";
            int time;
            cin>>time;
            cout<<endl;
            cout<<"Enter number of iterations for the Simulation: ";
            int iter;
            cin>>iter;
            cout<<endl<<endl;
            ofstream fout;
            vector<Body>::iterator it1;
            try {
                fout.open("SIMULATION.txt");
                if(!fout.is_open()) throw '1';

                for(it1=bodies.begin();it1!=bodies.end();it1++){ //prints initial values
                    fout<<it1->getPx()<<"\t"<<it1->getPy()<<"\t";
                }
                fout<<endl;
                for(int i=0;i<iter;i++){
                    for(it1=bodies.begin();it1!=bodies.end();it1++){ //calculates accelerations of all objects
                        calcAcc(*it1,bodies);
                    }
                    for(it1=bodies.begin();it1!=bodies.end();it1++){
                        it1->newPos(1);
                        fout<<it1->getPx()<<"\t"<<it1->getPy()<<"\t";
                    }
                    fout<<endl;

                }
                cout<<"Simulation Results successfully saved, file name: SIMULATION.txt "<<endl;
                fout.close();
            } catch (char e) {
                if (e==1) cout<<"Error. Output file could not be opened."<<endl;
            }
            catch (...){
                cout<<"Unidentified Error. Try Again."<<endl;
            }
        }
        if(option==0){
            cin.clear();
            cin.ignore();
            cout<<"Enter a valid number: "<<endl<<endl;;
        }
        if (option==3){
            break;
        }
    }
}

void calcAcc(Body &body,vector<Body> bodies){
    body.ax=0;
    body.ay=0;
    vector<Body>::iterator iter;
    for(iter=bodies.begin();iter!=bodies.end();iter++){
        if(iter->name!=body.name){
            body.ax+=((G*iter->getMass())/(((iter->getPx()-body.getPx())*(iter->getPx()-body.getPx()))+((iter->getPy()-body.getPy())*(iter->getPy()-body.getPy()))))*cos(atan2(iter->getPy()-body.getPy(),iter->getPx()-body.getPx()));
            body.ay+=((G*iter->getMass())/(((iter->getPx()-body.getPx())*(iter->getPx()-body.getPx()))+((iter->getPy()-body.getPy())*(iter->getPy()-body.getPy()))))*sin(atan2(iter->getPy()-body.getPy(),iter->getPx()-body.getPx()));
        }
    }
}
