#include <iostream>

using namespace std;

/* This program gets an integer (k) and returns the first triangular number that has more than k divisors.
 * REQUIREMENTS:
 *  -Integer number greater than zero.
 * GUARANTEES:
 *  -The return of the first triangular number with more than k divisors.
 */

int main()
{

int nesimo=1,k,n = 1, cont=0;

bool flag=false;                            //Flag to stop the while loop whenever it reaches the input number
cout<<"Ingrese un numero entero positivo: "<<endl;
cin >> k;

while(flag==false){
    nesimo=n*(n+1)/2;                       // Formula used to calculate the nth triangular number.
    cout<< nesimo<<": ";
    for (int j=1;j<=nesimo;j++){            //Loop to calculate each divisor of the nth triangular number.
        if (nesimo%j==0){                   //If divisible, increment the counter and print it as divisor.
            cont++;
            cout<<j<<", ";
        }
    }
    cout<<endl;
    if(k<=cont){                             //If the number of divisors is greater than or equal to the input number break the loop
        flag=true;
    }
    else cont=0;                            //If not, restart the counter and go to the next triangular number.
    n++;
}
cout<<"El numero es: "<<nesimo<<" y tiene "<<cont<<" divisores"<<endl;

}
