#include "graphsphere.h"

graphSphere::graphSphere(float px, float py, float rad, float m, float k): scale(1)
{
    esf= new sphereSim(px,py,rad,m,k);
}
graphSphere::~graphSphere(){
    delete esf;
}
QRectF graphSphere::boundingRect() const{
    return QRectF(-1*scale*esf->getRad(),-1*scale*esf->getRad(),2*scale*esf->getRad(),2*scale*esf->getRad());
}
void graphSphere::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    painter->setBrush(Qt::darkCyan);
    painter->drawEllipse(boundingRect());
}
void graphSphere::setScale(float s){
    scale=s;
}

void graphSphere::pintar()
{
    QBrush br(Qt::SolidPattern);
    br.setColor(Qt::black);


}
void graphSphere::actualizar(float dt, float v_lim){
    esf->actualizar(dt);
    setPos(esf->getPx()*scale,(v_lim-esf->getPy())*scale);
}
sphereSim *graphSphere::getSphere(){
    return esf;
}
