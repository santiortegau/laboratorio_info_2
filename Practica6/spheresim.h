#ifndef SPHERESIM_H
#define SPHERESIM_H

class sphereSim
{
public:
    sphereSim(float px, float py, float rad, float m, float k);
    ~sphereSim();
    void setVel(float px, float py);
    void setPos(float px, float py);
    void actualizar(float dt);
    float getPx() const;
    float getPy() const;
    float getRad() const;
    float getVx() const;
    float getVy() const;
    float getAx() const;
    float getAy() const;
    float getK() const;
    float getMass() const;
    void setAx(float x);
    void setAy(float y);

private:
    float x;
    float y;
    const float r;
    float vx;
    float vy;
    float ax;
    float ay;
    const float mass;
    const float K;


};

#endif // SPHERESIM_H
