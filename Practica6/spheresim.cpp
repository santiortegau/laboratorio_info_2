
#include "spheresim.h"


sphereSim::sphereSim(float px, float py, float rad, float m, float k): x(px),y(py),r(rad),vx(0),vy(0),ax(0),ay(0),mass(m),K(k)
{

}
sphereSim::~sphereSim(){

}
void sphereSim::setVel(float px, float py){
    vx=px;
    vy=py;
}
void sphereSim::setPos(float px, float py){
    x=px;
    y=py;
    vx=0;
    vy=0;
}
void sphereSim::actualizar(float dt){
    vx+=ax*dt;
    vy+=ay*dt;
    x+=vx*dt+(ax*dt*dt)/2;
    y+=vy*dt+(ay*dt*dt)/2;
}
float sphereSim::getPx() const{
    return x;
}
float sphereSim::getPy() const {
    return y;
}
float sphereSim::getRad() const{
    return r;
}
float sphereSim::getVx() const{
    return vx;
}
float sphereSim::getVy() const{
    return vy;
}
float sphereSim::getAx() const{
    return ax;
}
float sphereSim::getAy() const{
    return ay;
}
float sphereSim::getK() const{
    return K;
}
float sphereSim::getMass() const{
    return mass;
}

void sphereSim::setAx(float x)
{
    ax=x;
}

void sphereSim::setAy(float y)
{
    ay=y;
}
