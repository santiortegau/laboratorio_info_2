#ifndef GRAPHSPHERE_H
#define GRAPHSPHERE_H
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include "spheresim.h"



class graphSphere: public QGraphicsItem
{
public:
    graphSphere(float px, float py, float rad, float m, float k);
    ~graphSphere();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setScale(float s);
    void pintar();
    void actualizar(float dt, float v_lim);
    sphereSim* getSphere();
private:
    sphereSim* esf;
    float scale;

};

#endif // GRAPHSPHERE_H
