#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>
#include "cstdlib"
#include <ctime>
#define g 9.8

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    srand(time(NULL));
    ui->setupUi(this);
    h_limit=900;
    v_limit=400;
    dt=0.008;
    timer= new QTimer (this);
    spawnTimer=new QTimer;
    connect(spawnTimer,SIGNAL(timeout()),this,SLOT(spawnSphere()));
    connect(spawnTimer,SIGNAL(timeout()),this,SLOT(spawnSpecial()));
    connect(spawnTimer,SIGNAL(timeout()),this,SLOT(spawnAObst()));
    connect(spawnTimer,SIGNAL(timeout()),this,SLOT(spawnBObst()));
    spawnTimer->start(2000);
    scene=new QGraphicsScene(this);
    scene->setSceneRect(0,0,h_limit,v_limit);
    ui->graphicsView->setScene(scene);
    ui->centralWidget->adjustSize();
    scene->addRect(scene->sceneRect());
    timer->stop();
    connect(timer,SIGNAL(timeout()),this,SLOT(update()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete timer;
    delete scene;
}
void MainWindow::update(){
    for (int i=0;i<balls.size();i++){
        borderCollision(balls.at(i),balls);
        sphereCollision(balls.at(i),balls);
        calcAcc(balls);
        balls.at(i)->actualizar(dt,v_limit);
    } 
    for(int i=0;i<special.size();i++){
        borderCollision(special.at(i),special);
        specialSphereCollision(special.at(i));
        calcAcc(special);
        special.at(i)->actualizar(dt,v_limit);
    }

}
void MainWindow::borderCollision(graphSphere *b,QList<graphSphere*>balls){
    if(b->getSphere()->getPx()<b->getSphere()->getRad() || b->getSphere()->getPx()>h_limit-b->getSphere()->getRad()){
        b->getSphere()->setVel(-1*b->getSphere()->getVx(),b->getSphere()->getVy());
    }
    if(b->getSphere()->getPy()<b->getSphere()->getRad() || b->getSphere()->getPy()>v_limit-b->getSphere()->getRad()){
        b->getSphere()->setVel(b->getSphere()->getVx(),-1*b->getSphere()->getVy());
    }
    if(b->getSphere()->getPy()<b->getSphere()->getRad()){
        b->hide();
    }
}
void MainWindow::sphereCollision(graphSphere *b,QList<graphSphere*>balls){
    QList<QGraphicsRectItem*>::iterator iter2;
    for(iter2=aobst.begin();iter2!=aobst.end();iter2++){
        if(b->collidesWithItem(iter2.i->t())){
            b->getSphere()->setVel(b->getSphere()->getVx()*3,-3*b->getSphere()->getVy());
        }
    }
    QList<QGraphicsRectItem*>::iterator iter;
    for(iter=bobst.begin();iter!=bobst.end();iter++){
        if(b->collidesWithItem(iter.i->t())){
            b->getSphere()->setVel(b->getSphere()->getVx()*0.8,-1*b->getSphere()->getVy()*0.8);
        }
    }
}

void MainWindow::specialSphereCollision(graphSphere *b)
{
    QList<QGraphicsRectItem*>::iterator iter3;
    for(iter3=aobst.begin();iter3!=aobst.end();iter3++){
        if(b->collidesWithItem(iter3.i->t())){
            b->hide();
            b->setPos(1000,1000);
            b->getSphere()->setVel(0,0);
            iter3.i->t()->hide();
            iter3.i->t()->setPos(1000,1000);
            return;
        }
    }
    QList<QGraphicsRectItem*>::iterator iter;
    for(iter=bobst.begin();iter!=bobst.end();iter++){
        if(b->collidesWithItem(iter.i->t())){
            b->hide();
            b->setPos(1000,1000);
            b->getSphere()->setVel(0,0);
            iter.i->t()->hide();
            iter.i->t()->setPos(1000,1000);
            return;
        }
    }

}

void MainWindow::calcAcc(QList<graphSphere *> balls)
{
    for (int i=0;i<balls.size();i++){
        balls.at(i)->getSphere()->setAx((-1)*((balls.at(i)->getSphere()->getK()*((balls.at(i)->getSphere()->getVx()*balls.at(i)->getSphere()->getVx())+(balls.at(i)->getSphere()->getVy()*balls.at(i)->getSphere()->getVy()))*(balls.at(i)->getSphere()->getRad()*balls.at(i)->getSphere()->getRad())*(cos(atan2(balls.at(i)->getSphere()->getVy(),balls.at(i)->getSphere()->getVx()))))/balls.at(i)->getSphere()->getMass()));
        balls.at(i)->getSphere()->setAy(((-1)*((balls.at(i)->getSphere()->getK()*((balls.at(i)->getSphere()->getVx()*balls.at(i)->getSphere()->getVx())+(balls.at(i)->getSphere()->getVy()*balls.at(i)->getSphere()->getVy()))*(balls.at(i)->getSphere()->getRad()*balls.at(i)->getSphere()->getRad())*(sin(atan2(balls.at(i)->getSphere()->getVy(),balls.at(i)->getSphere()->getVx()))))/balls.at(i)->getSphere()->getMass()))-g);
    }
}

void MainWindow::on_pushButton_clicked()
{
    timer->start(10);
}

void MainWindow::spawnSphere()
{
    float px,mass,vy,vx;
    vx=20+rand()%100;
    vy=-1*(20+rand()%150);
    px=100+rand()%800;
    mass=20+rand()%500;
    balls.append(new graphSphere(px,390,10,mass,0.001));
    balls.last()->getSphere()->setVel(vx,vy);
    balls.last()->actualizar(0.01,400);
    scene->addItem(balls.last());
}

void MainWindow::spawnAObst()
{
    float px,py;
    px=100+rand()%700;
    py=100+rand()%290;
    QGraphicsRectItem *temp= new QGraphicsRectItem(px,py,40,10);
    temp->setBrush(QBrush(Qt::blue));
    scene->addItem(temp);
    aobst.append(temp);

}

void MainWindow::spawnBObst()
{
    float px,py;
    px=100+rand()%700;
    py=100+rand()%290;
    QGraphicsRectItem *temp= new QGraphicsRectItem(px,py,40,10);
    temp->setBrush(QBrush(Qt::red));
    this->scene->addItem(temp);
    bobst.append(temp);

}

void MainWindow::spawnSpecial()
{
    float px,mass,vy,vx;
    vx=20+rand()%100;
    vy=-1*(20+rand()%150);
    px=100+rand()%800;
    mass=20+rand()%500;
    graphSphere *Temp= new graphSphere(px,390,10,mass,0.001);
    special.append(Temp);
    special.last()->getSphere()->setVel(vx,vy);
    special.last()->actualizar(0.01,400);
    this->scene->addItem(special.last());

}
