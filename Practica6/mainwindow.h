#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include "graphsphere.h"
#include "spheresim.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void update();
    void on_pushButton_clicked();
    void spawnSphere();
    void spawnAObst();
    void spawnBObst();
    void spawnSpecial();


private:
    Ui::MainWindow *ui;
    QTimer *timer;
    QTimer *spawnTimer;
    QGraphicsScene *scene;
    QGraphicsItem *rect;
    float dt;
    int h_limit;
    int v_limit;
    void borderCollision(graphSphere *b,QList<graphSphere*>balls);
    void sphereCollision(graphSphere *b,QList<graphSphere*>balls);
    void specialSphereCollision(graphSphere *b);
    QList<graphSphere*> balls;
    void calcAcc(QList<graphSphere*> balls);
    QList<QGraphicsRectItem*> aobst;
    QList<QGraphicsRectItem*> bobst;
    QList<graphSphere*>special;
};

#endif // MAINWINDOW_H
