#include <iostream>

using namespace std;

/* This program calculates the minimum combination of bills and coins to return a desired value.
 * REQUIREMENTS:
 *  -Total value to return greater than zero.
 * GUARANTEES:
 *  -Prints each bill and coins denomination and its quantity according to the input value. If there're remainders it also prints them.
 */


int main()
{
  int n,b50,b20,b10,b5,b2,b1,m5,m2,m1,m50,acu; //Define variables for each coin and bill denomination
  cout <<"Ingrese un valor"<<endl;
  cin>>n;
  acu=n;
  b50=n/50000;      //To find the quantity of each bill denomination the input value is divided into the bill denomination
  acu-=(b50*50000); //Substract that value to the accumulator and continue to do the same
  cout<<"50.000: "<<b50<<endl;
  b20=acu/20000;
  acu-=b20*20000;
  cout<<"20.000: "<< b20<< endl;
  b10=acu/10000;
  acu-=b10*10000;
  cout<<"10.000: "<<b10<<endl;
  b5=acu/5000;
  acu-=b5*5000;
  cout<<"5.000: "<< b5<<endl;
  b2=acu/2000;
  acu-=b2*2000;
  cout<<"2.000: "<<b2<<endl;
  b1=acu/1000;
  acu-=b1*1000;
  cout<<"1.000: "<<b1<<endl;
  m5=acu/500;
  acu-=m5*500;
  cout<<"500: "<<m5<<endl;
  m2=acu/200;
  acu-=m2*200;
  cout<<"200: "<<m2<<endl;
  m1=acu/100;
  acu-=m1*100;
  cout<<"100: "<<m1<<endl;
  m50=acu/50;
  acu-=m50*50;
  cout<<"50: "<<m50<<endl;
  cout<<"Faltante: "<<acu<<endl;
   return 0;
  }
